#pragma once

#include <cmath>

namespace geom
{

	static constexpr double pi   = 3.14159265358979323846 ;
	static constexpr float  pi_f = 3.14159265358979323846f;

	template <typename T>
	T sqrt (T x)
	{
		return std::sqrt(x);
	}

	template <typename T>
	T frac (T x)
	{
		return x - std::floor(x);
	}

	template <typename T, typename P>
	P lerp (P const & p0, P const & p1, T const & t)
	{
		return p0 * (T(1) - t) + p1 * t;
	}

	template <typename T>
	T sqr (T const & x)
	{
		return x * x;
	}

	template <typename T>
	T cube (T const & x)
	{
		return x * x * x;
	}

	template <typename T>
	bool make_min (T & x, T const & y)
	{
		if (x > y)
		{
			x = y;
			return true;
		}
		return false;
	}

	template <typename T>
	bool make_max (T & x, T const & y)
	{
		if (x < y)
		{
			x = y;
			return true;
		}
		return false;
	}

	template <typename T>
	void compare_swap (T & x, T & y)
	{
		T xx = std::min(x, y);
		T yy = std::max(x, y);
		x = xx;
		y = yy;
	}

	template <typename T>
	bool same_sign (T x, T y)
	{
		return (x >= 0 && y >= 0) || (x <= 0 && y <= 0);
	}

}
