#pragma once

// TODO: use std::variant and require C++17
#include <boost/variant.hpp>

#include <iostream>

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/matrix.hpp>
#include <geom/primitives/interval.hpp>
#include <geom/primitives/rectangle.hpp>

// Symbolic expression type

namespace geom
{

	namespace detail
	{

		namespace sexpr
		{

			struct constant
			{
				double value;
			};

			struct variable
			{
				std::string name;
			};

			enum class unary_op_type
			{
				negation,
			};

			template <typename E1>
			struct unary_op
			{
				E1 arg1;
				unary_op_type op;
			};

			enum class binary_op_type
			{
				addition,
				substraction,
				multiplication,
				division,
			};

			template <typename E1, typename E2>
			struct binary_op
			{
				E1 arg1;
				E2 arg2;
				binary_op_type op;
			};

			using impl = boost::make_recursive_variant<
				constant,
				variable,
				unary_op<boost::recursive_variant_>,
				binary_op<boost::recursive_variant_, boost::recursive_variant_>
			>::type;

			inline auto make_unary_op (impl i1, unary_op_type op)
			{
				return unary_op<impl>{std::move(i1), op};
			}

			inline auto make_binary_op (impl i1, impl i2, binary_op_type op)
			{
				return binary_op<impl, impl>{std::move(i1), std::move(i2), op};
			}

			// NB: this in NOT intended to be an intelligent simplification routine,
			//     but only a helper for printing
			impl simplify (impl i);

		}

	}

	struct sexpr
	{
		detail::sexpr::impl impl;

		explicit sexpr (detail::sexpr::impl impl)
			: impl(detail::sexpr::simplify(std::move(impl)))
		{ }

		sexpr ( )
			: sexpr(0)
		{ }

		sexpr (int value)
			: impl(detail::sexpr::constant{static_cast<double>(value)})
		{ }

		sexpr (double value)
			: impl(detail::sexpr::constant{static_cast<double>(value)})
		{ }

		sexpr (float value)
			: impl(detail::sexpr::constant{static_cast<double>(value)})
		{ }

		sexpr (std::string name)
			: impl(detail::sexpr::variable{std::move(name)})
		{ }

		sexpr & operator += (sexpr const & other);
		sexpr & operator -= (sexpr const & other);
		sexpr & operator *= (sexpr const & other);
		sexpr & operator /= (sexpr const & other);

		template <std::size_t N>
		static geom::vector<sexpr, N> vector (std::string const & name)
		{
			geom::vector<sexpr, N> result;
			for (std::size_t i = 0; i < N; ++i)
				result[i] = sexpr(name + std::to_string(i));
			return result;
		}

		template <std::size_t N>
		static geom::point<sexpr, N> point (std::string const & name)
		{
			geom::point<sexpr, N> result;
			for (std::size_t i = 0; i < N; ++i)
				result[i] = sexpr(name + std::to_string(i));
			return result;
		}

		template <std::size_t R, std::size_t C>
		static geom::matrix<sexpr, R, C> matrix (std::string const & name)
		{
			geom::matrix<sexpr, R, C> result;
			for (std::size_t i = 0; i < R; ++i)
				for (std::size_t j = 0; j < C; ++j)
					result[i][j] = sexpr(name + std::to_string(i) + std::to_string(j));
			return result;
		}

		static geom::interval<sexpr> interval (std::string const & name)
		{
			return {sexpr(name + "_inf"), sexpr(name + "_sup")};
		}

		template <std::size_t N>
		static geom::rectangle<sexpr, N> rectangle (std::string const & name)
		{
			geom::rectangle<sexpr, N> result;
			for (std::size_t i = 0; i < N; ++i)
				result[i] = interval(name + std::to_string(i));
			return result;
		}
	};

	inline sexpr operator - (sexpr const & e1)
	{
		return sexpr(detail::sexpr::make_unary_op(e1.impl, detail::sexpr::unary_op_type::negation));
	}

	inline sexpr operator + (sexpr const & e1, sexpr const & e2)
	{
		return sexpr(detail::sexpr::make_binary_op(e1.impl, e2.impl, detail::sexpr::binary_op_type::addition));
	}

	inline sexpr operator - (sexpr const & e1, sexpr const & e2)
	{
		return sexpr(detail::sexpr::make_binary_op(e1.impl, e2.impl, detail::sexpr::binary_op_type::substraction));
	}

	inline sexpr operator * (sexpr const & e1, sexpr const & e2)
	{
		return sexpr(detail::sexpr::make_binary_op(e1.impl, e2.impl, detail::sexpr::binary_op_type::multiplication));
	}

	inline sexpr operator / (sexpr const & e1, sexpr const & e2)
	{
		return sexpr(detail::sexpr::make_binary_op(e1.impl, e2.impl, detail::sexpr::binary_op_type::division));
	}

	inline sexpr & sexpr::operator += (sexpr const & other)
	{
		return *this = *this + other;
	}

	inline sexpr & sexpr::operator -= (sexpr const & other)
	{
		return *this = *this - other;
	}

	inline sexpr & sexpr::operator *= (sexpr const & other)
	{
		return *this = *this * other;
	}

	inline sexpr & sexpr::operator /= (sexpr const & other)
	{
		return *this = *this / other;
	}

	std::ostream & operator << (std::ostream & os, sexpr const & e);

}
