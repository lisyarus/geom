#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/matrix.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	struct translation
	{
		using vector_type = geom::vector<T, D>;
		using point_type = point<T, D>;
		using homogeneous_matrix_type = geom::matrix<T, D + 1, D + 1>;

		translation ( );
		translation (vector_type v);

		vector_type vector ( ) const;
		vector_type vector (vector_type v);

		vector_type operator () (vector_type v) const;
		point_type operator () (point_type p) const;

		homogeneous_matrix_type homogeneous_matrix ( ) const;

		translation inverse ( ) const;

	private:
		vector_type v_;
	};

	template <typename T, std::size_t D>
	translation<T, D>::translation ( )
		: translation(vector_type::zero())
	{ }

	template <typename T, std::size_t D>
	translation<T, D>::translation (vector_type v)
		: v_(v)
	{ }

	template <typename T, std::size_t D>
	vector<T, D> translation<T, D>::vector ( ) const
	{
		return v_;
	}

	template <typename T, std::size_t D>
	vector<T, D> translation<T, D>::vector (vector_type v)
	{
		auto t = v_;
		v_ = v;
		return t;
	}

	template <typename T, std::size_t D>
	vector<T, D> translation<T, D>::operator () (vector_type v) const
	{
		return v;
	}

	template <typename T, std::size_t D>
	point<T, D> translation<T, D>::operator () (point_type p) const
	{
		return p + v_;
	}

	template <typename T, std::size_t D>
	matrix<T, D + 1, D + 1> translation<T, D>::homogeneous_matrix ( ) const
	{
		homogeneous_matrix_type m = homogeneous_matrix_type::identity();
		for (std::size_t i = 0; i < D; ++i)
			m[i][D] = v_[i];
		return m;
	}

	template <typename T, std::size_t D>
	translation<T, D> translation<T, D>::inverse ( ) const
	{
		return {-v_};
	}

}
