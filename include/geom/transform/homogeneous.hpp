#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/matrix.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	vector<T, D+1> homogeneous (vector<T, D> const & v)
	{
		vector<T, D+1> result;
		for (std::size_t i = 0; i < D; ++i)
			result[i] = v[i];
		result[D] = T(0);
		return result;
	}

	template <typename T, std::size_t D>
	vector<T, D+1> homogeneous (point<T, D> const & v)
	{
		vector<T, D+1> result;
		for (std::size_t i = 0; i < D; ++i)
			result[i] = v[i];
		result[D] = T(1);
		return result;
	}

	template <typename T, std::size_t D>
	matrix<T, D+1, D+1> homogeneous (matrix<T, D, D> const & m)
	{
		matrix<T, D+1, D+1> result = matrix<T, D+1, D+1>::zero();
		for (std::size_t i = 0; i < D; ++i)
		{
			for (std::size_t j = 0; j < D; ++j)
			{
				result[i][j] = m[i][j];
			}
		}
		result[D][D] = T(1);
		return result;
	}

}
