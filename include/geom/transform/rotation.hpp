#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/matrix.hpp>
#include <geom/transform/homogeneous.hpp>

namespace geom
{

	// Rotation in oriented plane (i,j)
	template <typename T, std::size_t D>
	struct plane_rotation
	{
		using scalar_type = T;
		using vector_type = vector<T, D>;
		using point_type = point<T, D>;
		using matrix_type = geom::matrix<T, D, D>;

		plane_rotation (std::size_t i, std::size_t j, T angle = T(0));

		scalar_type angle ( ) const;
		scalar_type angle (scalar_type a);

		vector_type operator () (vector_type v) const;
		point_type operator () (point_type p) const;

		matrix_type matrix ( ) const;

		plane_rotation inverse ( ) const;

	private:
		std::size_t i_, j_;
		T angle_;
	};

	// Equivalent to plane_rotation<T,2>(0,1)
	template <typename T>
	struct rotation_2
	{
		using scalar_type = T;
		using vector_type = geom::vector<T, 2>;
		using point_type = point<T, 2>;
		using matrix_type = geom::matrix<T, 2, 2>;

		rotation_2 ( );
		rotation_2 (scalar_type a);

		scalar_type angle ( ) const;
		scalar_type angle (scalar_type a);

		vector_type operator () (vector_type v) const;
		point_type operator () (point_type p) const;

		matrix_type matrix ( ) const;

		rotation_2 inverse ( ) const;

	private:
		scalar_type a_;
	};

	// 3D-rotation around an axis
	template <typename T>
	struct rotation_3
	{
		using scalar_type = T;
		using vector_type = geom::vector<T, 3>;
		using point_type = point<T, 3>;
		using matrix_type = geom::matrix<T, 3, 3>;

		rotation_3 ( );
		rotation_3 (vector_type axis, scalar_type angle);

		vector_type axis ( ) const;
		vector_type axis (vector_type a);

		scalar_type angle ( ) const;
		scalar_type angle (scalar_type a);

		vector_type operator () (vector_type v) const;
		point_type operator () (point_type p) const;

		matrix_type matrix ( ) const;

		rotation_3 inverse ( ) const;

	private:
		vector_type axis_;
		scalar_type angle_;
	};

	template <typename T, std::size_t D>
	plane_rotation<T, D>::plane_rotation (std::size_t i, std::size_t j, T angle)
		: i_(i), j_(j), angle_(angle)
	{ }

	template <typename T, std::size_t D>
	T plane_rotation<T, D>::angle ( ) const
	{
		return angle_;
	}

	template <typename T, std::size_t D>
	T plane_rotation<T, D>::angle (T a)
	{
		T t = angle_;
		angle_ = a;
		return t;
	}

	template <typename T, std::size_t D>
	vector<T, D> plane_rotation<T, D>::operator () (vector_type v) const
	{
		T vi = v[i_] * std::cos(angle_) - v[j_] * std::sin(angle_);
		T vj = v[i_] * std::sin(angle_) + v[j_] * std::cos(angle_);
		v[i_] = vi;
		v[j_] = vj;
		return v;
	}

	template <typename T, std::size_t D>
	point<T, D> plane_rotation<T, D>::operator () (point_type p) const
	{
		T pi = p[i_] * std::cos(angle_) - p[j_] * std::sin(angle_);
		T pj = p[i_] * std::sin(angle_) + p[j_] * std::cos(angle_);
		p[i_] = pi;
		p[j_] = pj;
		return p;
	}

	template <typename T, std::size_t D>
	matrix<T, D, D> plane_rotation<T, D>::matrix ( ) const
	{
		matrix_type m = matrix_type::identity();
		m[i_][i_] =  std::cos(angle_);
		m[i_][j_] = -std::sin(angle_);
		m[j_][i_] =  std::sin(angle_);
		m[j_][j_] =  std::cos(angle_);
		return m;
	}

	template <typename T, std::size_t D>
	plane_rotation<T, D> plane_rotation<T, D>::inverse ( ) const
	{
		return {i_, j_, -angle_};
	}

	template <typename T>
	rotation_2<T>::rotation_2 ( )
		: rotation_2(T(0))
	{ }

	template <typename T>
	rotation_2<T>::rotation_2 (scalar_type a)
		: a_(a)
	{ }

	template <typename T>
	T rotation_2<T>::angle ( ) const
	{
		return a_;
	}

	template <typename T>
	T rotation_2<T>::angle (scalar_type a)
	{
		auto t = a_;
		a_ = a;
		return t;
	}

	template <typename T>
	vector<T, 2> rotation_2<T>::operator () (vector_type v) const
	{
		return
		{
			std::cos(a_) * v[0] - std::sin(a_) * v[1],
			std::sin(a_) * v[0] + std::cos(a_) * v[1],
		};
	}

	template <typename T>
	point<T, 2> rotation_2<T>::operator () (point_type p) const
	{
		return
		{
			std::cos(a_) * p[0] - std::sin(a_) * p[1],
			std::sin(a_) * p[0] + std::cos(a_) * p[1],
		};
	}

	template <typename T>
	matrix<T, 2, 2> rotation_2<T>::matrix ( ) const
	{
		matrix_type m = matrix_type::identity();
		T c = std::cos(a_);
		T s = std::sin(a_);
		m[0][0] = c;
		m[0][1] = -s;
		m[1][0] = s;
		m[1][1] = c;
		return m;
	}

	template <typename T>
	rotation_2<T> rotation_2<T>::inverse ( ) const
	{
		return {-a_};
	}

	template <typename T>
	rotation_3<T>::rotation_3 ( )
		: rotation_3(vector_type{T(1), T(0), T(0)}, T(0))
	{ }

	template <typename T>
	rotation_3<T>::rotation_3 (vector_type axis, scalar_type angle)
		: axis_(axis)
		, angle_(angle)
	{ }

	template <typename T>
	vector<T, 3> rotation_3<T>::axis ( ) const
	{
		return axis_;
	}

	template <typename T>
	vector<T, 3> rotation_3<T>::axis (vector_type a)
	{
		auto t = axis_;
		axis_ = a;
		return t;
	}

	template <typename T>
	T rotation_3<T>::angle ( ) const
	{
		return angle_;
	}

	template <typename T>
	T rotation_3<T>::angle (scalar_type a)
	{
		auto t = angle_;
		angle_ = a;
		return t;
	}

	template <typename T>
	vector<T, 3> rotation_3<T>::operator () (vector_type v) const
	{
		return matrix() * v;
	}

	template <typename T>
	point<T, 3> rotation_3<T>::operator () (point_type p) const
	{
		point_type o = point_type::zero();
		return o + matrix() * (p - o);
	}

	template <typename T>
	matrix<T, 3, 3> rotation_3<T>::matrix ( ) const
	{
		matrix_type m = matrix_type::identity();
		T c = std::cos(angle_);
		T s = std::sin(angle_);
		T x = axis_[0];
		T y = axis_[1];
		T z = axis_[2];
		m[0][0] = c + x * x * (1 - c);
		m[0][1] = x * y * (1 - c) - z * s;
		m[0][2] = x * z * (1 - c) + y * s;
		m[1][0] = y * x * (1 - c) + z * s;
		m[1][1] = c + y * y * (1 - c);
		m[1][2] = y * z * (1 - c) - x * s;
		m[2][0] = z * x * (1 - c) - y * s;
		m[2][1] = z * y * (1 - c) + x * s;
		m[2][2] = c + z * z * (1 - c);
		return m;
	}

	template <typename T>
	rotation_3<T> rotation_3<T>::inverse ( ) const
	{
		return {axis_, -angle_};
	}

}
