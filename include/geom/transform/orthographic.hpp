#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/rectangle.hpp>
#include <geom/primitives/matrix.hpp>
#include <geom/transform/homogeneous.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	struct orthographic
	{
		using vector_type = vector<T, D>;
		using point_type = point<T, D>;
		using homogeneous_matrix_type = matrix<T, D + 1, D + 1>;
		using rectangle_type = rectangle<T, D>;

		orthographic ( );
		orthographic (rectangle_type r);

		rectangle_type box ( ) const;
		rectangle_type box (rectangle_type r);

		vector_type operator () (vector_type v) const;
		point_type operator () (point_type p) const;

		homogeneous_matrix_type homogeneous_matrix ( ) const;

		orthographic inverse ( ) const;

	private:
		rectangle_type r_;
	};

	template <typename T, std::size_t D>
	orthographic<T, D>::orthographic ( )
		: orthographic({{ { -1, 1 }, { -1, 1 }, { -1, 1 } }})
	{ }

	template <typename T, std::size_t D>
	orthographic<T, D>::orthographic (rectangle_type r)
		: r_(r)
	{ }

	template <typename T, std::size_t D>
	rectangle<T, D> orthographic<T, D>::box ( ) const
	{
		return r_;
	}

	template <typename T, std::size_t D>
	rectangle<T, D> orthographic<T, D>::box (rectangle<T, D> r)
	{
		std::swap(r, r_);
		return r;
	}

	template <typename T, std::size_t D>
	vector<T, D> orthographic<T, D>::operator () (vector<T, D> v) const
	{
		return homogeneous_matrix() * homogeneous(v);
	}

	template <typename T, std::size_t D>
	point<T, D> orthographic<T, D>::operator () (point<T, D> v) const
	{
		return homogeneous_matrix() * homogeneous(v);
	}

	template <typename T, std::size_t D>
	matrix<T, D + 1, D + 1> orthographic<T, D>::homogeneous_matrix ( ) const
	{
		auto m = matrix<T, D + 1, D + 1>::zero();

		for (std::size_t d = 0; d < D; ++d)
			m[d][d] = T(2) / (r_[d].sup - r_[d].inf);

		for (std::size_t d = 0; d < D; ++d)
			m[d][D] = - (r_[d].sup + r_[d].inf) / (r_[d].sup - r_[d].inf);

		m[D][D] = T(1);

		return m;
	}

}
