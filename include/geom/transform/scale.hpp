#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/matrix.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	struct scale
	{
		using vector_type = geom::vector<T, D>;
		using point_type = point<T, D>;
		using matrix_type = geom::matrix<T, D, D>;

		scale ( );
		scale (T v);
		scale (vector_type v);

		vector_type vector ( ) const;
		vector_type vector (vector_type v);

		vector_type operator () (vector_type v) const;
		point_type operator () (point_type p) const;

		matrix_type matrix ( ) const;

		scale inverse ( ) const;

	private:
		vector_type v_;
	};

	template <typename T, std::size_t D>
	scale<T, D>::scale ( )
	{
		for (auto & x : v_)
			x = T(1);
	}

	template <typename T, std::size_t D>
	scale<T, D>::scale (T v)
	{
		for (auto & x : v_)
			x = v;
	}

	template <typename T, std::size_t D>
	scale<T, D>::scale (vector_type v)
		: v_(v)
	{ }

	template <typename T, std::size_t D>
	vector<T, D> scale<T, D>::vector ( ) const
	{
		return v_;
	}

	template <typename T, std::size_t D>
	vector<T, D> scale<T, D>::vector (vector_type v)
	{
		auto t = v_;
		v_ = v;
		return t;
	}

	template <typename T, std::size_t D>
	vector<T, D> scale<T, D>::operator () (vector_type v) const
	{
		for (std::size_t i = 0; i < D; ++i)
			v[i] *= v_[i];
		return v;
	}

	template <typename T, std::size_t D>
	point<T, D> scale<T, D>::operator () (point_type p) const
	{
		for (std::size_t i = 0; i < D; ++i)
			p[i] *= v_[i];
		return p;
	}

	template <typename T, std::size_t D>
	matrix<T, D, D> scale<T, D>::matrix ( ) const
	{
		matrix_type m = matrix_type::identity();
		for (std::size_t i = 0; i < D; ++i)
			m[i][i] = v_[i];
		return m;
	}

	template <typename T, std::size_t D>
	scale<T, D> scale<T, D>::inverse ( ) const
	{
		vector_type v;
		for (std::size_t i = 0; i < D; ++i)
			v[i] = T(1) / v_[i];
		return {v};
	}

}
