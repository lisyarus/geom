#pragma once

#include <geom/primitives/matrix.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	struct swap
	{
		using vector_type = geom::vector<T, D>;
		using point_type = geom::point<T, D>;
		using matrix_type = geom::matrix<T, D, D>;

		swap (std::size_t i, std::size_t j);

		vector_type operator () (vector_type v) const;

		matrix_type matrix ( ) const;

		swap inverse ( ) const;

	private:
		std::size_t i_, j_;
	};

	template <typename T, std::size_t D>
	swap<T, D>::swap (std::size_t i, std::size_t j)
		: i_(i)
		, j_(j)
	{ }

	template <typename T, std::size_t D>
	geom::vector<T, D> swap<T, D>::operator () (vector_type v) const
	{
		std::swap(v[i_], v[j_]);
		return v;
	}

	template <typename T, std::size_t D>
	geom::matrix<T, D, D> swap<T, D>::matrix ( ) const
	{
		matrix_type m = matrix_type::identity();
		m[i_][i_] = 0;
		m[j_][j_] = 0;
		m[i_][j_] = 1;
		m[j_][i_] = 1;
		return m;
	}

	template <typename T, std::size_t D>
	swap<T, D> swap<T, D>::inverse ( ) const
	{
		return *this;
	}

}
