#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/functional.hpp>

#include <functional>
#include <cmath>
#include <optional>

namespace geom
{

	// vector * vector (dot product)
	template <typename T, std::size_t D>
	T operator * (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return functional::fold(std::plus<T>(), functional::map<vector<T, D>>(std::multiplies<T>(), v1, v2), T(0));
	}

	// vector ^ vector (cross product)
	// NB: only 3-dimensional
	template <typename T>
	vector<T, 3> operator ^ (vector<T, 3> const & v1, vector<T, 3> const & v2)
	{
		return
		{
			v1[1] * v2[2] - v1[2] * v2[1],
			v1[2] * v2[0] - v1[0] * v2[2],
			v1[0] * v2[1] - v1[1] * v2[0],
		};
	}

	// squared euclidean norm
	template <typename T, std::size_t D>
	T norm_sqr (vector<T, D> const & v)
	{
		return v * v;
	}

	// euclidean norm
	template <typename T, std::size_t D>
	T norm (vector<T, D> const & v)
	{
		return std::sqrt(norm_sqr(v));
	}

	// normalized vector
	template <typename T, std::size_t D>
	vector<T, D> normalized (vector<T, D> const & v)
	{
		return v / norm(v);
	}

	// projection of vector on direction
	template <typename T, std::size_t D>
	vector<T, D> projection (vector<T, D> const & v, vector<T, D> const & d)
	{
		auto const n = normalized(d);
		return n * (v * n);
	}

	// Gram-Schmidt process
	// Converts any basis to an orthonormal basis

	inline void gram_schmidt ( ) { }

	template <typename Vector, typename ... Vectors>
	void gram_schmidt (Vector & v, Vectors & ... vs)
	{
		v = normalized(v);

		using helper = int[];

		(void) helper { 0, (vs -= v * (v * vs), 0)... };

		gram_schmidt(vs...);
	}

	// Generates a vector orhogonal to given
	// NB: continuous implementation in odd dimension is
	//     prohibited by the hairy ball theorem

	template <typename T, std::size_t N>
	typename std::enable_if<N % 2 == 0, vector<T, N>>::type
		ort (vector<T, N> v)
	{
		for (std::size_t i = 0; i < N; i += 2)
		{
			v[i] = - v[i];
			std::swap(v[i], v[i + 1]);
		}
		return v;
	}

	template <typename T, std::size_t N>
	typename std::enable_if<N % 2 == 1, vector<T, N>>::type
		ort (vector<T, N> v)
	{
		std::optional<std::size_t> min_index;

		using std::abs;

		for (std::size_t i = 0; i < N; ++i)
		{
			if (!min_index)
				min_index = i;
			else if (abs(v[i]) < abs(v[*min_index]))
				min_index = i;
		}

		v[*min_index] = T();

		auto shifted_index = [&min_index](std::size_t i){
			return (i < *min_index) ? i : i + 1;
		};

		for (std::size_t i = 0; i + 1 < N; i += 2)
		{
			std::size_t i1 = shifted_index(i);
			std::size_t i2 = shifted_index(i + 1);
			v[i1] = -v[i1];
			std::swap(v[i1], v[i2]);
		}

		return v;
	}

	namespace detail
	{

		constexpr std::size_t combination_count (std::size_t n, std::size_t k)
		{
			std::size_t r = 1;
			for (std::size_t i = 0; i < k; ++i)
			{
				r *= (n - i);
				r /= (i + 1);
			}
			return r;
		}

		template <std::size_t I, typename Seq>
		struct index_sequence_append_impl;

		template <std::size_t I, std::size_t ... Js>
		struct index_sequence_append_impl<I, std::index_sequence<Js...>>
		{
			using type = std::index_sequence<I, Js...>;
		};

		template <std::size_t I, typename Seq>
		using index_sequence_append = typename index_sequence_append_impl<I, Seq>::type;

		template <std::size_t I, typename Seq>
		struct index_sequence_remove_impl;

		template <std::size_t I>
		struct index_sequence_remove_impl<I, std::index_sequence<>>
		{
			using type = std::index_sequence<>;
		};

		template <std::size_t I, std::size_t J, std::size_t ... Js>
		struct index_sequence_remove_impl<I, std::index_sequence<J, Js...>>
		{
			using type = index_sequence_append<J, typename index_sequence_remove_impl<I, std::index_sequence<Js...>>::type>;
		};

		template <std::size_t I, std::size_t ... Js>
		struct index_sequence_remove_impl<I, std::index_sequence<I, Js...>>
		{
			using type = typename index_sequence_remove_impl<I, std::index_sequence<Js...>>::type;
		};

		template <std::size_t I, typename Seq>
		using index_sequence_remove = typename index_sequence_remove_impl<I, Seq>::type;

		template <typename Sequence, std::size_t Shift>
		struct index_sequence_add_impl;

		template <std::size_t ... I, std::size_t Shift>
		struct index_sequence_add_impl<std::index_sequence<I...>, Shift>
		{
			using type = std::index_sequence<(I+Shift)...>;
		};

		template <typename Sequence, std::size_t Shift>
		using index_sequence_add = typename index_sequence_add_impl<Sequence, Shift>::type;

		template <typename Sequence>
		struct index_sequence_reverse_impl;

		template <>
		struct index_sequence_reverse_impl<std::index_sequence<>>
		{
			using type = std::index_sequence<>;
		};

		template <std::size_t I, typename Sequence>
		struct index_sequence_reverse_impl_helper;

		template <std::size_t I>
		struct index_sequence_reverse_impl_helper<I, std::index_sequence<>>
		{
			using type = std::index_sequence<I>;
		};

		template <std::size_t I, std::size_t ... Is>
		struct index_sequence_reverse_impl_helper<I, std::index_sequence<Is...>>
		{
			using type = std::index_sequence<Is..., I>;
		};

		template <std::size_t I, std::size_t ... Is>
		struct index_sequence_reverse_impl<std::index_sequence<I, Is...>>
			: index_sequence_reverse_impl_helper<I, typename index_sequence_reverse_impl<std::index_sequence<Is...>>::type>
		{ };

		template <typename Sequence>
		using index_sequence_reverse = typename index_sequence_reverse_impl<Sequence>::type;

		template <typename Sequence1, typename Sequence2>
		struct index_sequence_concat_two_impl;

		template <std::size_t ... I, std::size_t ... J>
		struct index_sequence_concat_two_impl<std::index_sequence<I...>, std::index_sequence<J...>>
		{
			using type = std::index_sequence<I..., J...>;
		};

		template <typename ... Sequences>
		struct index_sequence_concat_impl;

		template < >
		struct index_sequence_concat_impl<>
		{
			using type = std::index_sequence<>;
		};

		template <typename Sequence, typename ... Sequences>
		struct index_sequence_concat_impl<Sequence, Sequences...>
		{
			using type = typename index_sequence_concat_two_impl<Sequence, typename index_sequence_concat_impl<Sequences...>::type>::type;
		};

		template <typename ... Sequences>
		using index_sequence_concat = typename index_sequence_concat_impl<Sequences...>::type;

		template <typename Sequence1, typename Sequence2>
		struct index_sequence_difference_impl;

		template <typename Sequence1>
		struct index_sequence_difference_impl<Sequence1, std::index_sequence<>>
		{
			using type = Sequence1;
		};

		template <typename Sequence1, std::size_t I, std::size_t ... J>
		struct index_sequence_difference_impl<Sequence1, std::index_sequence<I, J...>>
		{
			using type = typename index_sequence_difference_impl<index_sequence_remove<I, Sequence1>, std::index_sequence<J...>>::type;
		};

		template <typename Sequence1, typename Sequence2>
		using index_sequence_difference = typename index_sequence_difference_impl<Sequence1, Sequence2>::type;

		template <std::size_t N, std::size_t K>
		struct last_combination
		{
			using type = index_sequence_add<std::make_index_sequence<K>, N - K>;
		};

		template <std::size_t N, typename Combination>
		struct prev_combination_reversed;

		template <std::size_t N, std::size_t K>
		struct prev_combination_reversed<N, std::index_sequence<K>>
		{
			using type = std::index_sequence<K-1>;
		};

		template <std::size_t N>
		struct prev_combination_reversed<N, std::index_sequence<0>>
		{
			using type = void;
		};

		template <std::size_t N, typename Combination>
		struct prev_combination_reversed_helper_append;

		template <std::size_t N, std::size_t ... Cs>
		struct prev_combination_reversed_helper_append<N, std::index_sequence<Cs...>>
		{
			using type = std::index_sequence<N, Cs...>;
		};

		template <std::size_t N>
		struct prev_combination_reversed_helper_append<N, void>
		{
			using type = void;
		};

		template <std::size_t N, bool Less, typename Combination>
		struct prev_combination_reversed_helper;

		template <std::size_t N, std::size_t C1, std::size_t C2, std::size_t ... Cs>
		struct prev_combination_reversed_helper<N, true, std::index_sequence<C1, C2, Cs...>>
		{
			using type = std::index_sequence<C1-1, C2, Cs...>;
		};

		template <std::size_t N, std::size_t C1, std::size_t C2, std::size_t ... Cs>
		struct prev_combination_reversed_helper<N, false, std::index_sequence<C1, C2, Cs...>>
			: prev_combination_reversed_helper_append<N-1, typename prev_combination_reversed<N-1, std::index_sequence<C2, Cs...>>::type>
		{ };

		template <std::size_t N, std::size_t C1, std::size_t C2, std::size_t ... Cs>
		struct prev_combination_reversed<N, std::index_sequence<C1, C2, Cs...>>
			: prev_combination_reversed_helper<N, (C1-1 > C2), std::index_sequence<C1, C2, Cs...>>
		{ };

		template <std::size_t N, typename Combination>
		struct prev_combination;

		template <typename Combination>
		struct prev_combination_helper
		{
			using type = index_sequence_reverse<Combination>;
		};

		template < >
		struct prev_combination_helper<void>
		{
			using type = void;
		};

		template <std::size_t N, typename Combination>
		struct prev_combination
			: prev_combination_helper<typename prev_combination_reversed<N, index_sequence_reverse<Combination>>::type>
		{ };

		template <std::size_t N, std::size_t K, typename ... Cs>
		struct combinations_impl;

		template <std::size_t N, std::size_t K, typename P, typename ... Cs>
		struct combinations_helper
		{
			using type = typename combinations_impl<N, K, P, Cs...>::type;
		};

		template <std::size_t N, std::size_t K, typename ... Cs>
		struct combinations_helper<N, K, void, Cs...>
		{
			using type = std::tuple<Cs...>;
		};

		template <std::size_t N, std::size_t K>
		struct combinations_impl<N, K>
		{
			using type = typename combinations_impl<N, K, typename last_combination<N, K>::type>::type;
		};

		template <std::size_t N, std::size_t K, typename C, typename ... Cs>
		struct combinations_impl<N, K, C, Cs...>
			: combinations_helper<N, K, typename prev_combination<N, C>::type, C, Cs...>
		{ };

		template <std::size_t N, std::size_t K>
		using combinations = typename combinations_impl<N, K>::type;

		template <std::size_t I, std::size_t ... J>
		struct permutation_inversions_impl_single;

		template <std::size_t I>
		struct permutation_inversions_impl_single<I>
		{
			static constexpr std::size_t value = 0;
		};

		template <std::size_t I, std::size_t J, std::size_t ... K>
		struct permutation_inversions_impl_single<I, J, K...>
		{
			static constexpr std::size_t value = (I > J ? 1 : 0) + permutation_inversions_impl_single<I, K...>::value;
		};

		template <typename Sequence>
		struct permutation_inversions_impl;

		template < >
		struct permutation_inversions_impl<std::index_sequence<>>
		{
			static constexpr std::size_t value = 0;
		};

		template <std::size_t I, std::size_t ... J>
		struct permutation_inversions_impl<std::index_sequence<I, J...>>
		{
			static constexpr std::size_t value = permutation_inversions_impl_single<I, J...>::value + permutation_inversions_impl<std::index_sequence<J...>>::value;
		};

		template <typename Sequence>
		constexpr std::size_t permutation_inversions = permutation_inversions_impl<Sequence>::value;

		template <typename Seq>
		struct index_sequence_complete;

		template <std::size_t I, typename T, std::size_t D>
		T signed_coordinate (vector<T, D> const & v)
		{
			return (I % 2 == 0) ? v[I] : -v[I];
		}

		template <typename T>
		T det_impl (std::index_sequence<>)
		{
			return T(1);
		}

		template <typename T, std::size_t I, std::size_t D>
		T det_impl (std::index_sequence<I>, vector<T, D> const & v)
		{
			return v[I];
		}

		template <typename T, std::size_t ... I, std::size_t D, typename ... Vectors>
		T det_impl (std::index_sequence<I...>, vector<T, D> const & v, Vectors const & ... vs)
		{
			static_assert(sizeof...(vs)+1 == sizeof...(I), "wrong number/dimension of vectors");
			static_assert(sizeof...(I) <= D, "wrong number/dimension of vectors");

			using seq = std::index_sequence<I...>;

			T result = T(0);

			using helper = int[];

			(void) helper { (result = v[I] * det_impl(index_sequence_remove<I, seq>(), vs...) - result, 0)..., 0 };

			return (sizeof...(I) % 2 == 0) ? -result : result;
		}

		template <std::size_t ... I, typename ... C, typename ... Vectors>
		auto wedge_impl (std::index_sequence<I...>, std::tuple<C...>, Vectors const & ... vs)
		{
			geom::vector<typename std::tuple_element_t<0, std::tuple<Vectors...>>::value_type, sizeof...(I)> result;

			using helper = int[];

			(void) helper { (result[I] = det_impl(C(), vs...), 0)..., 0 };

			return result;
		}

		template <std::size_t N, typename Combination>
		bool hodge_star_impl_helper_even ( )
		{
			using complement = index_sequence_difference<std::make_index_sequence<N>, Combination>;
			using permutation = index_sequence_concat<Combination, complement>;
			return permutation_inversions<permutation> % 2 == 0;
		}

		template <std::size_t N, std::size_t K, std::size_t ... I, typename Combinations, typename T, std::size_t L>
		auto hodge_star_impl (std::index_sequence<I...>, Combinations, vector<T, L> const & v)
		{
			vector<T, L> r;
			( ( r[L - I - 1] =  hodge_star_impl_helper_even<N, std::tuple_element_t<I, Combinations>>() ? v[I] : -v[I] ), ...);
			return r;
		}

	}

	template <typename T, std::size_t D, typename ... Vectors>
	auto det (vector<T, D> const & v, Vectors const & ... vs)
	{
		static_assert(sizeof...(Vectors) + 1 == D, "wrong number of vectors for det");
		return detail::det_impl(std::make_index_sequence<sizeof...(vs) + 1>(), v, vs...);
	}

	template <typename ... Vectors>
	auto wedge (Vectors const & ... vs)
	{
		static constexpr std::size_t D = std::tuple_element_t<0, std::tuple<Vectors...>>::dimension;
		static_assert(sizeof...(Vectors) <= D, "too many vectors for exterior_product");
		using C = detail::combinations<D, sizeof...(Vectors)>;
		return detail::wedge_impl(std::make_index_sequence<std::tuple_size<C>::value>(), C(), vs...);
	}

	// the Hodge star of a wedge product of K N-dimensional vectors
	template <std::size_t N, std::size_t K, typename T, std::size_t L>
	auto hodge_star (vector<T, L> const & v)
	{
		static_assert(L == detail::combination_count(N, K), "wrong dimension of wedge product");
		using C = detail::combinations<N, K>;
		return detail::hodge_star_impl<N, K>(std::make_index_sequence<std::tuple_size<C>::value>{}, C{}, v);
	}

	// Hodge star composed with wedge product
	template <typename ... Vectors>
	auto hodge_wedge (Vectors const & ... vs)
	{
		static constexpr std::size_t N = std::tuple_element_t<0, std::tuple<Vectors...>>::dimension;
		static constexpr std::size_t K = sizeof...(Vectors);

		return hodge_star<N, K>(wedge(vs...));
	}

}
