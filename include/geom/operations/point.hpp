#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/operations/vector.hpp>
#include <geom/functional.hpp>

#include <functional>
#include <cmath>

namespace geom
{

	// squared euclidean distance
	template <typename T, std::size_t D>
	T distance_sqr (point<T, D> const & p1, point<T, D> const & p2)
	{
		return norm_sqr(p1 - p2);
	}

	// euclidean distance
	template <typename T, std::size_t D>
	T distance (point<T, D> const & p1, point<T, D> const & p2)
	{
		return norm(p1 - p2);
	}

	template <typename T, std::size_t D, typename ... Points>
	auto det (point<T, D> const & p, Points const & ... ps)
	{
		auto const d = det((ps - p)...);
		return ((D % 2) == 0) ? d : -d;
	}

}
