#pragma once

#include <geom/primitives/matrix.hpp>
#include <geom/operations/vector.hpp>

namespace geom
{

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, C, R> transpose (matrix<T, R, C> const & m)
	{
		matrix<T, C, R> t;
		for (std::size_t r = 0; r < R; ++r)
			for (std::size_t c = 0; c < C; ++c)
				t[c][r] = m[r][c];
		return t;
	}

	namespace detail
	{

		template <typename T, std::size_t D, std::size_t ... I>
		T det_impl (matrix<T, D, D> const & m, std::index_sequence<I...>)
		{
			return det(m[I]...);
		}

	}

	template <typename T, std::size_t D>
	T det (matrix<T, D, D> const & m)
	{
		return detail::det_impl(m, std::make_index_sequence<D>());
	}

	namespace detail
	{

		template <typename T, std::size_t D, std::size_t I, std::size_t J, std::size_t ... Is>
		void cofactor_impl_element (matrix<T, D, D> const & m, matrix<T, D, D> & i, std::index_sequence<Is...>)
		{
			using Js = index_sequence_remove<J, std::make_index_sequence<D>>;

			i[I][J] = det_impl<T>(Js(), m[Is]...);

			if (((I + J) % 2) == 1)
				i[I][J] = - i[I][J];
		}

		template <typename T, std::size_t D, std::size_t I, std::size_t ... J>
		void cofactor_impl_row (matrix<T, D, D> const & m, matrix<T, D, D> & i, std::index_sequence<J...>)
		{
			using Is = index_sequence_remove<I, std::make_index_sequence<D>>;

			using helper = int[];

			(void) helper { 0, (cofactor_impl_element<T, D, I, J>(m, i, Is()), 0)... };
		}

		template <typename T, std::size_t D, std::size_t ... I>
		matrix<T, D, D> cofactor_impl (matrix<T, D, D> const & m, std::index_sequence<I...>)
		{
			matrix<T, D, D> i;

			using helper = int[];

			(void) helper { 0, (cofactor_impl_row<T, D, I>(m, i, std::make_index_sequence<D>()), 0)... };

			return i;
		}

	}

	template <typename T, std::size_t D>
	matrix<T, D, D> cofactor (matrix<T, D, D> const & m)
	{
		return detail::cofactor_impl(m, std::make_index_sequence<D>());
	}

	template <typename T, std::size_t D>
	matrix<T, D, D> adjugate (matrix<T, D, D> const & m)
	{
		return transpose(cofactor(m));
	}

	template <typename T, std::size_t D>
	matrix<T, D, D> inverse (matrix<T, D, D> const & m)
	{
		return adjugate(m) / det(m);
	}

}
