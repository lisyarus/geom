#pragma once

#include <geom/primitives/interval.hpp>

namespace geom
{

	template <typename T>
	T clamp (T x, interval<T> const & i)
	{
		if (x < i.inf)
			x = i.inf;
		if (x > i.sup)
			x = i.sup;
		return x;
	}

	template <typename T>
	bool contains (interval<T> const & i, T const & x)
	{
		return (i.inf <= x) && (x <= i.sup);
	}

	template <typename T>
	T signed_distance (T const & x, interval<T> const & i)
	{
		if (x < i.inf)
			return i.inf - x;
		else if (x > i.sup)
			return x - i.sup;
		else
			return -std::min(x - i.inf, i.sup - x);
	}

	template <typename T>
	T distance (T const & x, interval<T> const & i)
	{
		if (x < i.inf)
			return i.inf - x;
		else if (x > i.sup)
			return x - i.sup;
		else
			return T(0);
	}

}
