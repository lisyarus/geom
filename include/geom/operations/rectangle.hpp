#pragma once

#include <geom/operations/interval.hpp>
#include <geom/primitives/rectangle.hpp>
#include <geom/utility.hpp>

namespace geom
{

	template <typename P, typename T, std::size_t D>
	P clamp (P p, rectangle<T, D> const & r)
	{
		for (std::size_t i = 0; i < D; ++i)
			p[i] = clamp(p[i], r[i]);
		return p;
	}

	template <typename P, typename T, std::size_t D>
	bool contains (rectangle<T, D> const & r, P const & p)
	{
		for (std::size_t i = 0; i < D; ++i)
			if (!contains(r[i], p[i]))
				return false;
		return true;
	}

	template <typename T, std::size_t D>
	T distance_outside (point<T, D> const & p, rectangle<T, D> const & r)
	{
		T s(0);
		for (std::size_t i = 0; i < D; ++i)
		{
			s += sqr(distance(p[i], r[i]));
		}
		return std::sqrt(s);
	}

	template <typename T, std::size_t D>
	T signed_distance (point<T, D> const & p, rectangle<T, D> const & r)
	{
		if (contains(r, p))
		{
			T d = detail::limits<T>::min();
			for (std::size_t i = 0; i < D; ++i)
			{
				T di = distance(p[i], r[i]);
				make_max(d, di);
			}
			return d;
		}
		else
		{
			return distance_outside(p, r);
		}
	}

	template <typename T, std::size_t D>
	T distance (point<T, D> const & p, rectangle<T, D> const & r)
	{
		if (contains(r, p))
		{
			return T(0);
		}
		else
		{
			return distance_outside(p, r);
		}
	}
}
