#pragma once

#ifdef GEOM_EXACT_PREDICATES
#	include <boost/multiprecision/number.hpp>
#	include <boost/multiprecision/gmp.hpp>
#endif

#include <geom/operations/vector.hpp>
#include <geom/operations/point.hpp>

namespace geom
{

	enum class orientation_t
	{
		left,
		collinear,
		right,
	};

	template <typename T>
	orientation_t orientation (point<T, 2> const & p1, point<T, 2> const & p2, point<T, 2> const & p3)
	{
#ifdef GEOM_EXACT_PREDICATES
		auto const v12 = p2 - p1;
		auto const v13 = p3 - p1;

		T const l = v12[0] * v13[1];
		T const r = v12[1] * v13[0];

		T const res = l - r;

		T const eps = (std::abs(l) + std::abs(r)) * 8 * std::numeric_limits<T>::epsilon();

		if (res > eps)
			return orientation_t::left;
		if (res < -eps)
			return orientation_t::right;

		typedef boost::multiprecision::number<boost::multiprecision::gmp_float<0>> prec_t;

		auto const pp1 = cast<prec_t>(p1);
		auto const pp2 = cast<prec_t>(p2);
		auto const pp3 = cast<prec_t>(p3);

		prec_t const prec_res = det(pp2 - pp1, pp3 - pp1);
		if (prec_res > prec_t(0))
			return orientation_t::left;
		if (prec_res < prec_t(0))
			return orientation_t::right;
		return orientation_t::collinear;

#else

		T const res = det(p1, p2, p3);
		if (res > T(0))
			return orientation_t::left;
		if (res < T(0))
			return orientation_t::right;
		return orientation_t::collinear;

#endif
	}

	template <typename T, std::size_t N, typename ... Points>
	orientation_t orientation (point<T, N> const & p0, Points const & ... ps)
	{
		T const res = det(p0, ps...);
		if (res > T(0))
			return orientation_t::left;
		if (res < T(0))
			return orientation_t::right;
		return orientation_t::collinear;
	}

	template <typename ... Args>
	bool left (Args const & ... args)
	{
		return orientation(args...) == orientation_t::left;
	}

	template <typename ... Args>
	bool collinear (Args const & ... args)
	{
		return orientation(args...) == orientation_t::collinear;
	}

	template <typename ... Args>
	bool right (Args const & ... args)
	{
		return orientation(args...) == orientation_t::right;
	}

}
