#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/functional.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	bool operator == (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return functional::compare_equal(v1, v2);
	}

	template <typename T, std::size_t D>
	bool operator != (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return !(v1 == v2);
	}

	template <typename T, std::size_t D>
	bool operator < (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return functional::compare_less(v1, v2);
	}

	template <typename T, std::size_t D>
	bool operator <= (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return !(v2 < v1);
	}

	template <typename T, std::size_t D>
	bool operator > (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return v2 < v1;
	}

	template <typename T, std::size_t D>
	bool operator >= (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return !(v1 < v2);
	}

}
