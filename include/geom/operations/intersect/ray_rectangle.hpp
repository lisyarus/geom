#pragma once

#include <geom/primitives/ray.hpp>
#include <geom/primitives/rectangle.hpp>
#include <geom/functional.hpp>
#include <geom/utility.hpp>

#include <numeric>

namespace geom
{

	// Returns a range [t1,t2] containing all t such that
	//  r(t) is contained in b
	// Can be empty in case of no intersection
	template <typename T, std::size_t D>
	interval<T> intersect (ray<T, D> const & r, rectangle<T, D> const & b)
	{
		interval<T> i[D];

		for (std::size_t d = 0; d < D; ++d)
		{
			i[d].inf = (b[d].inf - r.origin[d]) / r.direction[d];
			i[d].sup = (b[d].sup - r.origin[d]) / r.direction[d];

			compare_swap(i[d].inf, i[d].sup);
		}

		return std::accumulate(std::begin(i), std::end(i), interval<T>::full(), functional::bit_and());
	}

}
