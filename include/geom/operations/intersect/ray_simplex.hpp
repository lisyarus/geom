#pragma once

#include <geom/primitives/ray.hpp>
#include <geom/primitives/simplex.hpp>
#include <geom/primitives/interval.hpp>
#include <geom/operations/vector.hpp>
#include <geom/utility.hpp>

#include <tuple>
#include <utility>
#include <numeric>

namespace geom
{

	namespace detail
	{

		template <std::size_t I, typename SubstVector, typename ... Vectors>
		auto det_subst (SubstVector const & s, Vectors const & ... vs)
		{
			std::tuple<Vectors ...> t { vs ... };
			std::get<I>(t) = s;
			return std::apply([](auto const & ... vs){ return det(vs...); }, t);
		}


		template <typename T, std::size_t D, std::size_t ... I>
		interval<T> intersect (ray<T, D + 1> const & r, simplex<point<T, D + 1>, D> const & s, std::index_sequence<I...>)
		{
			T a[D + 1];

			T const v = det((s[I] - r.origin) ...);

			((a[I] = det_subst<I>(r.direction, (s[I] - r.origin) ...)), ...);

			T const sa = std::accumulate(std::begin(a), std::end(a), T(0));

			if (std::all_of(std::begin(a), std::end(a), [sa](T const & a){ return same_sign(a, sa); }))
				return interval<T>{v / sa};
			else
				return {};
		}

	}

	template <typename T, std::size_t D>
	interval<T> intersect (ray<T, D + 1> const & r, simplex<point<T, D + 1>, D> const & s)
	{
		return detail::intersect(r, s, std::make_index_sequence<D + 1>{});
	}

}
