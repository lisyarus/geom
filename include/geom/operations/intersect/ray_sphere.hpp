#pragma once

#include <geom/primitives/ray.hpp>
#include <geom/primitives/sphere.hpp>
#include <geom/primitives/interval.hpp>
#include <geom/operations/vector.hpp>
#include <geom/utility.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	interval<T> intersect (ray<T, D + 1> const & r, sphere<T, D> const & s)
	{
		T const a = norm_sqr(r.direction);
		T const b_2 = r.direction * (r.origin - s.origin);
		T const c = norm_sqr(r.origin - s.origin) - sqr(s.radius);

		T const d = b_2 * b_2 - a * c;

		if (d < 0)
			return {};

		T const sqrtD = sqrt(d);

		T const t1 = (- b_2 - sqrtD) / a;
		T const t2 = (- b_2 + sqrtD) / a;

		return {t1, t2};
	}

}
