#pragma once

#include <geom/primitives/ray.hpp>
#include <geom/primitives/hyperplane.hpp>
#include <geom/primitives/interval.hpp>

namespace geom
{

	// Returns a singular range [t] such that r(t) is the point of intersection
	// Can be empty (if there is no actual intersection)
	// or infinite (if the ray is parallel to the hyperplane)
	template <typename T, std::size_t D>
	interval<T> intersect (ray<T, D> const & r, hyperplane<T, D> const & h)
	{
		auto t = - h(r.origin) / h(r.direction);
		if (t < 0)
			return {};
		else
			return interval<T>{t};
	}

}
