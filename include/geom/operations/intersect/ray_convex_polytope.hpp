#pragma once

#include <geom/primitives/ray.hpp>
#include <geom/primitives/interval.hpp>
#include <geom/primitives/convex_polytope.hpp>
#include <geom/operations/intersect/ray_hyperplane.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	T intersect (ray<T, D> const & r, convex_polytope<T, D> const & c)
	{
		interval<T> result;

		for (std::size_t i = 0; i < c.planes.size(); ++i)
		{
			auto t = intersect(r, c.planes[i]);
			auto p = r(t);

			interval<T> contained;

			for (std::size_t j = 0; j < c.planes.size(); ++j)
			{
				// Essential to prevent floating-point error issues
				if (j == i) continue;

				contained |= interval(c.planes[j](p));
			}

			if (T(0) <= contained.inf)
				result |= interval(t);
		}

		return result.inf;
	}

}
