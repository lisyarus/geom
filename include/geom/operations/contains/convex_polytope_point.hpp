#pragma once

#include <geom/primitives/point.hpp>
#include <geom/primitives/interval.hpp>
#include <geom/primitives/convex_polytope.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	bool contains (convex_polytope<T, D> const & c, point<T, D> const & p)
	{
		interval<T> i;
		for (auto const & h : c.planes)
			i |= interval(h(p));
		return T(0) <= i.inf;
	}

}
