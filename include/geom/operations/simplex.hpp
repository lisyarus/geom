#pragma once

#include <geom/primitives/simplex.hpp>
#include <geom/operations/vector.hpp>

namespace geom
{

	namespace detail
	{

		inline constexpr std::size_t factorial (std::size_t n)
		{
			std::size_t f = 1;
			while (n --> 0)
				f *= (n+1);
			return f;
		}

		template <typename P, std::size_t N, std::size_t ... I>
		auto measure_sqr (simplex<P, N> const & s, std::index_sequence<I...>)
		{
			auto f = factorial(N);
			return geom::norm_sqr(geom::wedge((s[I+1] - s[0])...)) / (f*f);
		}

	}

	template <typename P, std::size_t N>
	auto measure_sqr (simplex<P, N> const & s)
	{
		return detail::measure_sqr(s, std::make_index_sequence<N>());
	}

	template <typename P, std::size_t N>
	auto measure (simplex<P, N> const & s)
	{
		return std::sqrt(measure_sqr(s));
	}

	template <typename P>
	auto length (segment<P> const & s)
	{
		return measure(s);
	}

	template <typename P>
	auto area (triangle<P> const & s)
	{
		return measure(s);
	}

	template <typename P>
	auto volume (tetrahedron<P> const & s)
	{
		return measure(s);
	}

}
