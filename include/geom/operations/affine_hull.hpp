#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/hyperplane.hpp>
#include <geom/operations/vector.hpp>
#include <geom/transform/homogeneous.hpp>

namespace geom
{

	// Affine hull of N N-dimensional points
	template <typename P, typename ... Points>
	auto affine_hull (P const & p, Points const & ... points)
	{
		static constexpr std::size_t N = 1 + sizeof...(Points);
		using scalar_type = typename P::value_type;

		hyperplane<scalar_type, N> result { homogeneous(hodge_wedge( ( points - p ) ... )) };
		result += (p - P::zero());
		return result;
	}

}
