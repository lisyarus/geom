#pragma once

#include <geom/primitives/interval.hpp>

namespace geom
{

	namespace detail
	{

		template <typename T>
		struct interval_iterator
		{
			T value;

			T operator * ( ) const
			{
				return value;
			}

			interval_iterator & operator ++ ( )
			{
				++value;
				return *this;
			}

			friend bool operator == (interval_iterator<T> const & i1, interval_iterator<T> const & i2)
			{
				return i1.value == i2.value;
			}

			friend bool operator != (interval_iterator<T> const & i1, interval_iterator<T> const & i2)
			{
				return !(i1 == i2);
			}
		};

		template <typename T>
		struct interval_range
		{
			interval<T> i;

			interval_iterator<T> begin ( ) const
			{
				return {i.inf};
			}

			interval_iterator<T> end ( ) const
			{
				return {i.sup};
			}
		};

	}

	template <typename T>
	detail::interval_range<T> range (interval<T> const & i)
	{
		return {i};
	}

}
