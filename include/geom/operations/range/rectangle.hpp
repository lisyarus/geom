#pragma once

#include <geom/primitives/rectangle.hpp>
#include <geom/primitives/point.hpp>
#include <geom/operations/compare/point.hpp>

namespace geom
{

	namespace detail
	{

		template <typename T, std::size_t D>
		struct rectangle_iterator
		{
			geom::point<T, D> value;
			geom::rectangle<T, D> & rect;

			geom::point<T, D> operator * ( )
			{
				return value;
			}

			rectangle_iterator & operator ++ ( )
			{
				for (std::size_t i = 0; i < D; ++i)
				{
					++value[i];
					if (value[i] < rect[i].sup)
						break;
					else if (i + 1 != D)
					{
						value[i] = rect[i].inf;
					}
				}

				return *this;
			}

			friend bool operator == (rectangle_iterator<T, D> const & i1, rectangle_iterator<T, D> const & i2)
			{
				return i1.value == i2.value;
			}

			friend bool operator != (rectangle_iterator<T, D> const & i1, rectangle_iterator<T, D> const & i2)
			{
				return !(i1 == i2);
			}
		};

		template <typename T, std::size_t D>
		struct rectangle_range
		{
			rectangle<T, D> rect;

			rectangle_iterator<T, D> begin ( )
			{
				point<T, D> v;
				for (std::size_t i = 0; i < D; ++i)
					v[i] = rect[i].inf;
				return {v, rect};
			}

			rectangle_iterator<T, D> end ( )
			{
				point<T, D> v;
				for (std::size_t i = 0; i + 1 < D; ++i)
				{
					v[i] = rect[i].inf;
				}
				v[D - 1] = rect[D - 1].sup;
				return {v, rect};
			}
		};

	}

	template <typename T, std::size_t D>
	detail::rectangle_range<T, D> range (rectangle<T, D> const & r)
	{
		return {r};
	}

}
