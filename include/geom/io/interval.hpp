#pragma once

#include <geom/primitives/interval.hpp>

#include <iostream>
#include <type_traits>

namespace geom
{

	template <typename T>
	std::ostream & operator << (std::ostream & os, interval<T> const & i)
	{
		return os << '[' << i.inf << ", " << i.sup << ']';
	}

}
