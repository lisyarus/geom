#pragma once

#include <geom/primitives/rectangle.hpp>
#include <geom/io/interval.hpp>

#include <iostream>

namespace geom
{

	template <typename T, std::size_t D>
	std::ostream & operator << (std::ostream & os, rectangle<T, D> const & r)
	{
		for (std::size_t i = 0; i < D; ++i)
		{
			if (i > 0)
				os << 'x';
			os << r[i];
		}
		return os;
	}

}
