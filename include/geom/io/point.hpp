#pragma once

#include <geom/primitives/point.hpp>

#include <iostream>

namespace geom
{

	template <typename T, std::size_t D>
	std::ostream & operator << (std::ostream & os, point<T, D> const & p)
	{
		os << '(';
		for (std::size_t i = 0; i < D; ++i)
		{
			if (i != 0)
				os << ',' << ' ';
			os << p[i];
		}
		return os << ')';
	}

}
