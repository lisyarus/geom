#pragma once

#include <geom/primitives/vector.hpp>

#include <iostream>

namespace geom
{

	template <typename T, std::size_t D>
	std::ostream & operator << (std::ostream & os, vector<T, D> const & v)
	{
		os << '(';
		for (std::size_t i = 0; i < D; ++i)
		{
			if (i != 0)
				os << ',' << ' ';
			os << v[i];
		}
		return os << ')';
	}

}
