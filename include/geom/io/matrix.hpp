#pragma once

#include <geom/primitives/matrix.hpp>

#include <iostream>

namespace geom
{

	template <typename T, std::size_t R, std::size_t C>
	std::ostream & operator << (std::ostream & os, matrix<T, R, C> const & m)
	{
		os << '(';
		for (std::size_t i = 0; i < R; ++i)
		{
			if (i != 0)
				os << ',' << ' ';
			os << m[i];
		}
		return os << ')';
	}

	namespace detail
	{

		template <typename T, std::size_t R, std::size_t C>
		struct multiline_matrix_printer
		{
			matrix<T, R, C> const & m;

			friend std::ostream & operator << (std::ostream & os, multiline_matrix_printer p)
			{
				for (std::size_t r = 0; r < R; ++r)
				{
					os << "(";
					for (std::size_t c = 0; c < C; ++c)
						os << ' ' << p.m[r][c];
					os << " )\n";
				}
				return os;
			}
		};

	}

	template <typename T, std::size_t R, std::size_t C>
	detail::multiline_matrix_printer<T, R, C> multiline (matrix<T, R, C> const & m)
	{
		return {m};
	}

}
