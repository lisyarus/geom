#pragma once

#include <geom/array.hpp>

#include <functional>

namespace geom
{

namespace functional
{

	template <typename Result, typename F, typename ... Arrays>
	Result map (F const & f, Arrays const & ... a)
	{
		Result result;
		for (std::size_t i = 0; i < Result::dimension; ++i)
		{
			result[i] = f(a[i]...);
		}
		return result;
	}

	template <typename F, typename Array, typename R>
	R fold (F const & f, Array const & a, R init)
	{
		for (std::size_t i = 0; i < Array::dimension; ++i)
		{
			init = f(init, a[i]);
		}
		return init;
	}

	namespace detail
	{

		template <typename F, typename T>
		struct binder1
		{
			F f;
			T x;

			template <typename ... Args>
			auto operator() (Args const & ... args) const -> decltype(f(x, args...))
			{
				return f(x, args...);
			}
		};

		template <typename F, typename T>
		struct binder2
		{
			F f;
			T y;

			template <typename First, typename ... Args>
			auto operator() (First const & x, Args const & ... args) const -> decltype(f(x, y, args...))
			{
				return f(x, y, args...);
			}
		};

		struct negate
		{
			template <typename T>
			auto operator() (T const & x) const -> decltype(-x)
			{
				return -x;
			}
		};

		struct plus
		{
			template <typename T1, typename T2>
			auto operator() (T1 const & x1, T2 const & x2) const -> decltype(x1 + x2)
			{
				return x1 + x2;
			}
		};

		struct minus
		{
			template <typename T1, typename T2>
			auto operator() (T1 const & x1, T2 const & x2) const -> decltype(x1 - x2)
			{
				return x1 - x2;
			}
		};

		struct multiplies
		{
			template <typename T1, typename T2>
			auto operator() (T1 const & x1, T2 const & x2) const -> decltype(x1 * x2)
			{
				return x1 * x2;
			}
		};

		struct divides
		{
			template <typename T1, typename T2>
			auto operator() (T1 const & x1, T2 const & x2) const -> decltype(x1 / x2)
			{
				return x1 / x2;
			}
		};

		struct bit_and
		{
			template <typename T1, typename T2>
			auto operator() (T1 const & x1, T2 const & x2) const -> decltype(x1 & x2)
			{
				return x1 & x2;
			}
		};

		struct bit_or
		{
			template <typename T1, typename T2>
			auto operator() (T1 const & x1, T2 const & x2) const -> decltype(x1 | x2)
			{
				return x1 | x2;
			}
		};

	}

	template <typename T, typename H>
	struct cast
	{
		T operator() (H const & x) const noexcept
		{
			return static_cast<T>(x);
		}
	};

	inline detail::negate negate ( ) { return {}; }

	inline detail::plus plus ( ) { return {}; }
	inline detail::minus minus ( ) { return {}; }
	inline detail::multiplies multiplies ( ) { return {}; }
	inline detail::divides divides ( ) { return {}; }
	inline detail::bit_and bit_and ( ) { return {}; }
	inline detail::bit_or bit_or ( ) { return {}; }

	template <typename T>
	auto plus1 (T x) -> detail::binder1<detail::plus, T> { return {{}, x}; }

	template <typename T>
	auto minus1 (T x) -> detail::binder1<detail::minus, T> { return {{}, x}; }

	template <typename T>
	auto multiplies1 (T x) -> detail::binder1<detail::multiplies, T> { return {{}, x}; }

	template <typename T>
	auto divides1 (T x) -> detail::binder1<detail::divides, T> { return {{}, x}; }

	template <typename T>
	auto bit_and1 (T x) -> detail::binder1<detail::bit_and, T> { return {{}, x}; }

	template <typename T>
	auto bit_or1 (T x) -> detail::binder1<detail::bit_or, T> { return {{}, x}; }

	template <typename T>
	auto plus2 (T x) -> detail::binder2<detail::plus, T> { return {{}, x}; }

	template <typename T>
	auto minus2 (T x) -> detail::binder2<detail::minus, T> { return {{}, x}; }

	template <typename T>
	auto multiplies2 (T x) -> detail::binder2<detail::multiplies, T> { return {{}, x}; }

	template <typename T>
	auto divides2 (T x) -> detail::binder2<detail::divides, T> { return {{}, x}; }

	template <typename T>
	auto bit_and2 (T x) -> detail::binder2<detail::bit_and, T> { return {{}, x}; }

	template <typename T>
	auto bit_or2 (T x) -> detail::binder2<detail::bit_or, T> { return {{}, x}; }

	template <typename A1, typename A2>
	bool compare_equal (A1 const & a1, A2 const & a2)
	{
		static_assert(A1::dimension == A2::dimension, "dimensions should be equal");

		for (std::size_t i = 0; i < A1::dimension; ++i)
		{
			if (a1[i] != a2[i])
				return false;
		}

		return true;
	}

	template <typename A1, typename A2>
	bool compare_less (A1 const & a1, A2 const & a2)
	{
		static_assert(A1::dimension == A2::dimension, "dimensions should be equal");

		for (std::size_t i = 0; i < A1::dimension; ++i)
		{
			if (a1[i] < a2[i])
				return true;
			if (a1[i] > a2[i])
				return false;
		}

		return false;
	}

}

}
