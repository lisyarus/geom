#pragma once

#include <geom/operations/orientation.hpp>

#include <algorithm>

namespace geom
{

	template <typename Iterator, typename Point>
	bool convex_contains (Iterator begin, Iterator end, Point const & p)
	{
		if (right(*begin, *std::next(begin), p))
			return false;

		if (left(*begin, *std::prev(end, 1), p))
			return false;

		auto comp = [begin](Point const & p1, Point const & p2)
		{
			return left(*begin, p1, p2);
		};

		auto it = std::lower_bound(std::next(begin), end, p, comp);

		return !right(*std::prev(it), *it, p);
	}

}
