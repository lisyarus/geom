#pragma once

#include <geom/primitives/rectangle.hpp>

namespace geom
{

	// Bounding box of a range of points
	// Note: UB if begin == end
	template <typename InputIterator>
	auto bbox (InputIterator begin, InputIterator end)
	{
		using point_type = typename std::remove_reference<decltype(*begin)>::type;
		using rectangle_type = rectangle<typename point_type::value_type, point_type::dimension>;

		rectangle_type result = rectangle_type::singular(*begin);
		++begin;

		for (; begin != end; ++begin)
		{
			result |= *begin;
		}

		return result;
	}

	template <typename Range>
	auto bbox (Range && range)
	{
		return bbox(std::begin(range), std::end(range));
	}

	template <typename Point>
	auto bbox (std::initializer_list<Point> const & il)
	{
		return bbox(il.begin(), il.end());
	}

}
