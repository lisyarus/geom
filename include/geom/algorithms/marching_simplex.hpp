#pragma once

#include <geom/primitives/triangulation.hpp>

#include <unordered_map>

namespace geom
{

	namespace detail
	{

		inline void hash_combine (std::size_t & seed, std::size_t value)
		{
			 seed ^= value + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}

		template <typename T, std::size_t N>
		struct hash_array
		{
			std::size_t operator() (std::array<T, N> const & a) const
			{
				std::hash<T> hash;

				std::size_t h = 0;
				for (auto const & x : a)
					hash_combine(h, hash(x));
				return h;
			}
		};

		using edge_cache_t = std::unordered_map<std::array<std::size_t, 2>, std::size_t, detail::hash_array<std::size_t, 2>>;

		inline void sort_edge (std::array<std::size_t, 2> & e)
		{
			if (e[0] > e[1])
				std::swap(e[0], e[1]);
		}

	}

	template <typename Point, typename Function>
	std::vector<Point> marching_simplex (triangulation<Point, 2> const & t, Function && f)
	{
		std::vector<Point> result;

		using function_value_type = decltype(f(std::declval<Point>()));

		std::vector<function_value_type> function_values(t.vertices.size());
		for (std::size_t v = 0; v < t.vertices.size(); ++v)
			function_values[v] = f(t.vertices[v]);

		auto push = [&result](Point p0, Point p1){
			result.vertices.push_back(p0);
			result.vertices.push_back(p1);
		};

		for (auto const & s : t.simplices)
		{
			auto p = [&](std::size_t i0, std::size_t i1)
			{
				auto const & p0 = t.vertices[s[i0]];
				auto const & p1 = t.vertices[s[i1]];
				auto v0 = function_values[s[i0]];
				auto v1 = function_values[s[i1]];
				return p0 - (p1 - p0) * v0 / (v1 - v0);
			};

			int mask = 0;
			for (std::size_t i = 0; i < 3; ++i)
			{
				mask |= (function_values[s[i]] > 0) ? (1 << i) : 0;
			}

			switch (mask)
			{
			case 0b000:
				break;
			case 0b001:
				push(p(0, 2), p(0, 1));
				break;
			case 0b010:
				push(p(0, 1), p(1, 2));
				break;
			case 0b011:
				push(p(0, 2), p(1, 2));
				break;
			case 0b100:
				push(p(1, 2), p(0, 2));
				break;
			case 0b101:
				push(p(1, 2), p(0, 1));
				break;
			case 0b110:
				push(p(0, 1), p(0, 2));
				break;
			case 0b111:
				break;
			}
		}

		return result;
	}

	template <typename Point, typename Function>
	triangulation<Point, 1> marching_simplex (triangulation<Point, 2> const & t, Function && f, merge_tag_t)
	{
		using function_value_type = decltype(f(std::declval<Point>()));

		std::vector<function_value_type> function_values(t.vertices.size());
		for (std::size_t v = 0; v < t.vertices.size(); ++v)
			function_values[v] = f(t.vertices[v]);

		triangulation<Point, 1> result;

		detail::edge_cache_t edge_cache;

		auto push = [&result](std::size_t i0, std::size_t i1){
			result.simplices.push_back({i0, i1});
		};

		for (auto const & s : t.simplices)
		{
			auto p = [&](std::size_t i0, std::size_t i1)
			{
				std::size_t j0 = s[i0];
				std::size_t j1 = s[i1];

				std::array<std::size_t, 2> edge { j0, j1 };
				detail::sort_edge(edge);

				auto it = edge_cache.find(edge);
				if (it == edge_cache.end())
				{
					auto const & p0 = t.vertices[j0];
					auto const & p1 = t.vertices[j1];
					auto v0 = function_values[j0];
					auto v1 = function_values[j1];

					std::size_t index = result.vertices.size();
					result.vertices.push_back(p0 - (p1 - p0) * v0 / (v1 - v0));
					edge_cache[edge] = index;
					return index;
				}
				else
				{
					return it->second;
				}
			};

			int mask = 0;
			for (std::size_t i = 0; i < 3; ++i)
			{
				mask |= (function_values[s[i]] > 0) ? (1 << i) : 0;
			}

			switch (mask)
			{
			case 0b000:
				break;
			case 0b001:
				push(p(0, 2), p(0, 1));
				break;
			case 0b010:
				push(p(0, 1), p(1, 2));
				break;
			case 0b011:
				push(p(0, 2), p(1, 2));
				break;
			case 0b100:
				push(p(1, 2), p(0, 2));
				break;
			case 0b101:
				push(p(1, 2), p(0, 1));
				break;
			case 0b110:
				push(p(0, 1), p(0, 2));
				break;
			case 0b111:
				break;
			}
		}

		return result;
	}

	template <typename Point, typename Function>
	std::vector<Point> marching_simplex (triangulation<Point, 3> const & t, Function && f)
	{
		std::vector<Point> result;

		using function_value_type = decltype(f(std::declval<Point>()));

		std::vector<function_value_type> function_values(t.vertices.size());
		for (std::size_t v = 0; v < t.vertices.size(); ++v)
			function_values[v] = f(t.vertices[v]);

		auto push = [&result](Point p0, Point p1, Point p2){
			result.push_back(p0);
			result.push_back(p1);
			result.push_back(p2);
		};

		for (auto const & s : t.simplices)
		{
			auto p = [&](std::size_t i0, std::size_t i1)
			{
				auto const & p0 = t.vertices[s[i0]];
				auto const & p1 = t.vertices[s[i1]];
				auto v0 = function_values[s[i0]];
				auto v1 = function_values[s[i1]];
				return p0 - (p1 - p0) * v0 / (v1 - v0);
			};

			int mask = 0;
			for (std::size_t i = 0; i < 4; ++i)
			{
				mask |= (function_values[s[i]] > 0) ? (1 << i) : 0;
			}

			switch (mask)
			{
			case 0b0000:
				break;
			case 0b0001:
				push(p(0,1), p(0,2), p(0,3));
				break;
			case 0b0010:
				push(p(0,1), p(1,3), p(1,2));
				break;
			case 0b0011:
				push(p(0,2), p(0,3), p(1,3));
				push(p(0,2), p(1,3), p(1,2));
				break;
			case 0b0100:
				push(p(0,2), p(1,2), p(2,3));
				break;
			case 0b0101:
				push(p(2,3), p(0,3), p(0,1));
				push(p(2,3), p(0,1), p(1,2));
				break;
			case 0b0110:
				push(p(0,1), p(1,3), p(2,3));
				push(p(0,1), p(2,3), p(0,2));
				break;
			case 0b0111:
				push(p(0,3), p(1,3), p(2,3));
				break;
			case 0b1000:
				push(p(2,3), p(1,3), p(0,3));
				break;
			case 0b1001:
				push(p(0,2), p(2,3), p(0,1));
				push(p(2,3), p(1,3), p(0,1));
				break;
			case 0b1010:
				push(p(1,2), p(0,1), p(2,3));
				push(p(0,1), p(0,3), p(2,3));
				break;
			case 0b1011:
				push(p(2,3), p(1,2), p(0,2));
				break;
			case 0b1100:
				push(p(1,2), p(1,3), p(0,2));
				push(p(1,3), p(0,3), p(0,2));
				break;
			case 0b1101:
				push(p(1,2), p(1,3), p(0,1));
				break;
			case 0b1110:
				push(p(0,3), p(0,2), p(0,1));
				break;
			case 0b1111:
				break;
			}
		}

		return result;
	}


	template <typename Point, typename Function>
	triangulation<Point, 2> marching_simplex (triangulation<Point, 3> const & t, Function && f, merge_tag_t)
	{
		using function_value_type = decltype(f(std::declval<Point>()));

		std::vector<function_value_type> function_values(t.vertices.size());
		for (std::size_t v = 0; v < t.vertices.size(); ++v)
			function_values[v] = f(t.vertices[v]);

		triangulation<Point, 2> result;

		detail::edge_cache_t edge_cache;

		auto push = [&result](std::size_t i0, std::size_t i1, std::size_t i2){
			result.simplices.push_back({i0, i1, i2});
		};

		for (auto const & s : t.simplices)
		{
			auto p = [&](std::size_t i0, std::size_t i1)
			{
				std::size_t j0 = s[i0];
				std::size_t j1 = s[i1];

				std::array<std::size_t, 2> edge { j0, j1 };
				detail::sort_edge(edge);

				auto it = edge_cache.find(edge);
				if (it == edge_cache.end())
				{
					auto const & p0 = t.vertices[j0];
					auto const & p1 = t.vertices[j1];
					auto v0 = function_values[j0];
					auto v1 = function_values[j1];

					std::size_t index = result.vertices.size();
					result.vertices.push_back(p0 - (p1 - p0) * v0 / (v1 - v0));
					edge_cache[edge] = index;
					return index;
				}
				else
				{
					return it->second;
				}
			};

			int mask = 0;
			for (std::size_t i = 0; i < 4; ++i)
			{
				mask |= (function_values[s[i]] > 0) ? (1 << i) : 0;
			}

			switch (mask)
			{
			case 0b0000:
				break;
			case 0b0001:
				push(p(0,1), p(0,2), p(0,3));
				break;
			case 0b0010:
				push(p(0,1), p(1,3), p(1,2));
				break;
			case 0b0011:
				push(p(0,2), p(0,3), p(1,3));
				push(p(0,2), p(1,3), p(1,2));
				break;
			case 0b0100:
				push(p(0,2), p(1,2), p(2,3));
				break;
			case 0b0101:
				push(p(2,3), p(0,3), p(0,1));
				push(p(2,3), p(0,1), p(1,2));
				break;
			case 0b0110:
				push(p(0,1), p(1,3), p(2,3));
				push(p(0,1), p(2,3), p(0,2));
				break;
			case 0b0111:
				push(p(0,3), p(1,3), p(2,3));
				break;
			case 0b1000:
				push(p(2,3), p(1,3), p(0,3));
				break;
			case 0b1001:
				push(p(0,2), p(2,3), p(0,1));
				push(p(2,3), p(1,3), p(0,1));
				break;
			case 0b1010:
				push(p(1,2), p(0,1), p(2,3));
				push(p(0,1), p(0,3), p(2,3));
				break;
			case 0b1011:
				push(p(2,3), p(1,2), p(0,2));
				break;
			case 0b1100:
				push(p(1,2), p(1,3), p(0,2));
				push(p(1,3), p(0,3), p(0,2));
				break;
			case 0b1101:
				push(p(1,2), p(1,3), p(0,1));
				break;
			case 0b1110:
				push(p(0,3), p(0,2), p(0,1));
				break;
			case 0b1111:
				break;
			}
		}

		return result;
	}

}
