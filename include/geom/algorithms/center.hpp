#pragma once

#include <cstddef>

namespace geom
{

	// barycenter of a range of points
	// Note: UB if begin == end
	template <typename InputIterator>
	auto center (InputIterator begin, InputIterator end)
	{
		auto origin = *begin;
		using point_type = typename std::remove_reference<decltype(origin)>::type;
		using vector_type = typename point_type::vector_type;
		auto delta = vector_type::zero();

		++begin;

		std::size_t count = 1;

		for (; begin != end; ++begin, ++count)
		{
			delta += *begin - origin;
		}

		delta /= static_cast<typename point_type::value_type>(count);

		return origin + delta;
	}

}
