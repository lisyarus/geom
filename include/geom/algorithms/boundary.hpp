#pragma once

#include <geom/primitives/simplex.hpp>
#include <geom/primitives/triangulation.hpp>

namespace geom
{

	template <std::size_t N>
	abstract_triangulation<N-1> boundary (abstract_simplex<N>)
	{
		abstract_triangulation<N-1> result;

		for (std::size_t i = 0; i <= N; ++i)
		{
			std::array<std::size_t, N> b;
			for (std::size_t j = 0; j < i; ++j)
				b[j] = j;
			for (std::size_t j = i+1; j <= N; ++j)
				b[j-1] = j;
			result.simplices.push_back(b);
		}

		if (N > 1)
		{
			for (std::size_t i = 1; i <= N; i += 2)
			{
				std::swap(result.simplices[i][0], result.simplices[i][1]);
			}
		}

		return result;
	}

	template <typename P, std::size_t N>
	triangulation<P, N-1> boundary (simplex<P, N> const & s)
	{
		triangulation<P, N-1> result;
		result.vertices.insert(result.vertices.begin(), s.begin(), s.end());
		static_cast<abstract_triangulation<N-1> &>(result) = boundary(abstract_simplex<N>{});
		return result;
	}

}
