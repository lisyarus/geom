#pragma once

#include <geom/algorithms/convex_hull/quickhull_4d.hpp>

namespace geom
{

	template <typename T>
	std::vector<std::array<std::size_t, 4>> delaunay (std::vector<geom::point<T, 3>> const & points)
	{
		std::vector<geom::point<T, 4>> points_4d(points.size());
		for (std::size_t i = 0; i < points.size(); ++i)
		{
			points_4d[i] = {points[i][0], points[i][1], points[i][2],
				points[i][0] * points[i][0] + points[i][1] * points[i][1] + points[i][2] * points[i][2]};
		}

		auto result = quickhull(points_4d);

		auto const is_down = [&](std::array<std::size_t, 4> const & t)
		{
			return geom::orientation(points[t[0]], points[t[1]], points[t[2]], points[t[3]]) == geom::orientation_t::right;
		};

		result.erase(std::partition(result.begin(), result.end(), is_down), result.end());

		for (auto & t : result)
			std::swap(t[0], t[1]);

		return result;
	}

}
