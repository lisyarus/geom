#pragma once

#include <geom/algorithms/convex_hull/quickhull_3d.hpp>

namespace geom
{

	template <typename T>
	std::vector<std::array<std::size_t, 3>> delaunay (std::vector<geom::point<T, 2>> const & points)
	{
		std::vector<geom::point<T, 3>> points_3d(points.size());
		for (std::size_t i = 0; i < points.size(); ++i)
		{
			points_3d[i] = {points[i][0], points[i][1], points[i][0] * points[i][0] + points[i][1] * points[i][1]};
		}

		auto result = quickhull(points_3d);

		auto const is_down = [&](std::array<std::size_t, 3> const & t)
		{
			return geom::orientation(points[t[0]], points[t[1]], points[t[2]]) == geom::orientation_t::right;
		};

		result.erase(std::partition(result.begin(), result.end(), is_down), result.end());

		for (auto & t : result)
			std::swap(t[0], t[1]);

		return result;
	}

}
