#pragma once

#include <geom/operations/orientation.hpp>
#include <geom/operations/compare/point.hpp>

#include <vector>
#include <algorithm>

namespace geom
{

	namespace detail
	{

		template <typename Iterator, typename Point>
		Iterator in_place_quickhull_impl (Iterator begin, Iterator end, Point p1, Point p2)
		{
			auto size = std::distance(begin, end);

			if (size <= 1)
				return end;

			auto s_it = std::max_element(begin, end, [=](auto const & q1, auto const & q2){
				return det(q1, p1, p2) < det(q2, p1, p2);
			});

			auto s = *s_it;

			std::iter_swap(begin, s_it);

			auto left_end = std::partition(std::next(begin), end, [=](auto const & q){
				return left(p1, s, q);
			});

			auto right_end = std::partition(left_end, end, [=](auto const & q){
				return left(s, p2, q);
			});

			left_end = std::prev(left_end);

			std::iter_swap(begin, left_end);

			auto left_hull_end = in_place_quickhull_impl(begin, left_end, p1, s);

			std::iter_swap(left_hull_end, left_end);

			auto right_hull_end = in_place_quickhull_impl(std::next(left_end), right_end, s, p2);

			return std::copy(std::next(left_end), right_hull_end, std::next(left_hull_end));
		}

	}

	template <typename Iterator>
	Iterator in_place_quickhull (Iterator begin, Iterator end)
	{
		typedef typename std::iterator_traits<Iterator>::value_type point_type;

		if (std::distance(begin, end) < 3)
			return end;

		auto minmax_it = std::minmax_element(begin, end);

		auto p1 = *minmax_it.first;
		auto p2 = *minmax_it.second;

		if (minmax_it.second != begin)
		{
			std::iter_swap(begin, minmax_it.first);
			std::iter_swap(std::next(begin), minmax_it.second);
		}
		else
		{
			std::iter_swap(begin, minmax_it.first);
			std::iter_swap(std::next(begin), minmax_it.first);
		}

		auto upper_end = std::partition(std::next(begin, 2), end, [=](auto const & q){
			return left(p1, p2, q);
		});

		upper_end = std::prev(upper_end);

		std::iter_swap(std::next(begin), upper_end);

		auto upper_hull_end = detail::in_place_quickhull_impl(std::next(begin), upper_end, p1, p2);
		auto lower_hull_end = detail::in_place_quickhull_impl(std::next(upper_end), end, p2, p1);

		return std::copy(upper_end, lower_hull_end, upper_hull_end);
	}

	template <typename Iterator, typename OutputIterator>
	OutputIterator quickhull (Iterator begin, Iterator end, OutputIterator out)
	{
		std::vector<typename std::iterator_traits<Iterator>::value_type> points(begin, end);
		points.erase(in_place_quickhull(points.begin(), points.end()), points.end());
		return std::copy(points.rbegin(), points.rend(), out);
	}


}
