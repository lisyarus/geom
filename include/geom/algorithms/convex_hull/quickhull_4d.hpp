#pragma once

#include <geom/primitives/point.hpp>
#include <geom/operations/orientation.hpp>
#include <geom/operations/point.hpp>

#include <vector>
#include <list>
#include <queue>
#include <map>
#include <algorithm>

namespace geom
{

	template <typename T>
	std::vector<std::array<std::size_t, 4ul>> quickhull (std::vector<geom::point<T, 4ul>> const & points)
	{
		struct face;

		using handle = typename std::list<face>::iterator;
		using triangle = std::array<std::size_t, 3>;
		using tetrahedron = std::array<std::size_t, 4>;

		static auto const canonical = [](triangle t){
			std::size_t max_i = 0;
			if (t[1] > t[max_i]) max_i = 1;
			if (t[2] > t[max_i]) max_i = 2;

			std::size_t temp;
			switch (max_i)
			{
			case 0:
				break;
			case 1:
				temp = t[0];
				t[0] = t[1];
				t[1] = t[2];
				t[2] = temp;
				break;
			case 2:
				temp = t[0];
				t[0] = t[2];
				t[2] = t[1];
				t[1] = temp;
				break;
			}

			return t;
		};

		static auto dual = [](triangle t){
			return canonical({t[0], t[2], t[1]});
		};

		struct face
		{
			tetrahedron simplex;
			handle neigh[4]; // neigh[i] is opposite to vertex[i]
			bool valid;
			std::vector<std::size_t> out;
		};

		std::list<face> hull;
		std::list<face> deleted;

		handle const null = hull.end();

		std::queue<handle> face_queue;

		{
			std::size_t i0 = 0;
			std::size_t i1 = 1;
			std::size_t i2 = 2;
			std::size_t i3 = 3;
			std::size_t i4 = 4;

			if (geom::right(points[i0], points[i1], points[i2], points[i3], points[i4]))
				std::swap(i3, i4);

			tetrahedron t0 {     i1, i2, i3, i4 };
			tetrahedron t1 { i0,     i2, i3, i4 };
			tetrahedron t2 { i0, i1,     i3, i4 };
			tetrahedron t3 { i0, i1, i2,     i4 };
			tetrahedron t4 { i0, i1, i2, i3     };

			std::swap(t1[2], t1[3]);
			std::swap(t3[2], t3[3]);

			std::vector<std::size_t> out0, out1, out2, out3, out4;

			for (std::size_t i = 0; i < points.size(); ++i)
			{
				if (i == i0) continue;
				if (i == i1) continue;
				if (i == i2) continue;
				if (i == i3) continue;
				if (i == i4) continue;

				if (geom::right(points[t0[0]], points[t0[1]], points[t0[2]], points[t0[3]], points[i]))
					out0.push_back(i);
				else if (geom::right(points[t1[0]], points[t1[1]], points[t1[2]], points[t1[3]], points[i]))
					out1.push_back(i);
				else if (geom::right(points[t2[0]], points[t2[1]], points[t2[2]], points[t2[3]], points[i]))
					out2.push_back(i);
				else if (geom::right(points[t3[0]], points[t3[1]], points[t3[2]], points[t3[3]], points[i]))
					out3.push_back(i);
				else if (geom::right(points[t4[0]], points[t4[1]], points[t4[2]], points[t4[3]], points[i]))
					out4.push_back(i);
			}

			auto it0 = hull.insert(hull.end(), {t0, {}, true, std::move(out0)});
			auto it1 = hull.insert(hull.end(), {t1, {}, true, std::move(out1)});
			auto it2 = hull.insert(hull.end(), {t2, {}, true, std::move(out2)});
			auto it3 = hull.insert(hull.end(), {t3, {}, true, std::move(out3)});
			auto it4 = hull.insert(hull.end(), {t4, {}, true, std::move(out4)});

			it0->neigh[0] = it1;
			it0->neigh[1] = it2;
			it0->neigh[2] = it3;
			it0->neigh[3] = it4;

			it1->neigh[0] = it0;
			it1->neigh[1] = it2;
			it1->neigh[2] = it4;
			it1->neigh[3] = it3;

			it2->neigh[0] = it0;
			it2->neigh[1] = it1;
			it2->neigh[2] = it3;
			it2->neigh[3] = it4;

			it3->neigh[0] = it0;
			it3->neigh[1] = it1;
			it3->neigh[2] = it4;
			it3->neigh[3] = it2;

			it4->neigh[0] = it0;
			it4->neigh[1] = it1;
			it4->neigh[2] = it2;
			it4->neigh[3] = it3;

			face_queue.push(it0);
			face_queue.push(it1);
			face_queue.push(it2);
			face_queue.push(it3);
			face_queue.push(it4);
		}

		std::size_t iteration = 0;

		while (!face_queue.empty())
		{
			handle h = face_queue.front();
			face_queue.pop();

			if (!h->valid)
				continue;

			std::vector<std::size_t> const & out_set = h->out;

			if (out_set.empty())
				continue;

			tetrahedron const t = h->simplex;

			float dist = -std::numeric_limits<float>::infinity();
			std::size_t max_i = out_set[0];

			for (auto i : out_set)
			{
				float const d = geom::det(points[t[0]], points[t[1]], points[t[2]], points[t[3]], points[i]);

				if (d > dist)
				{
					dist = d;
					max_i = i;
				}
			}

			std::vector<handle> visible;

			std::queue<handle> visible_queue;
			visible_queue.push(h);
			h->valid = false;

			while (!visible_queue.empty())
			{
				auto h = visible_queue.front();
				visible_queue.pop();
				visible.push_back(h);

				for (std::size_t i : {0, 1, 2, 3})
				{
					handle it = h->neigh[i];
					auto const & t = it->simplex;
					if (it->valid && geom::right(points[t[0]], points[t[1]], points[t[2]], points[t[3]], points[max_i]))
					{
						it->valid = false;
						visible_queue.push(it);
					}
				}
			}

			std::vector<std::size_t> unassigned;
			std::vector<std::pair<handle, std::size_t>> boundary;

			for (auto h : visible)
			{
				auto & out = h->out;
				unassigned.insert(unassigned.end(), out.begin(), out.end());

				for (std::size_t i : {0, 1, 2, 3})
				{
					handle n = h->neigh[i];
					if (n->valid)
					{
						bool found = false;

						for (std::size_t j : {0, 1, 2, 3})
						{
							if (n->neigh[j] == h)
							{
								boundary.push_back({n, j});
								found = true;
								break;
							}
						}
					}
				}

				deleted.splice(deleted.end(), hull, h);
			}

			auto unassigned_end = std::prev(unassigned.end());

			std::iter_swap(std::find(unassigned.begin(), unassigned.end(), max_i), unassigned_end);

			std::map<triangle, std::pair<handle, std::size_t>> boundary_map;

			for (auto p : boundary)
			{
				handle const h = p.first;
				std::size_t const i = p.second;
				auto const & s = h->simplex;
				tetrahedron t;

				switch (i)
				{
				case 0:
					t = { s[1], s[2], s[3], max_i };
					break;
				case 1:
					t = { s[0], s[3], s[2], max_i };
					break;
				case 2:
					t = { s[0], s[1], s[3], max_i };
					break;
				case 3:
					t = { s[0], s[2], s[1], max_i };
					break;
				}

				auto it = std::partition(unassigned.begin(), unassigned_end, [&](std::size_t i){
					return geom::left(points[t[0]], points[t[1]], points[t[2]], points[t[3]], points[i]);
				});

				std::vector<std::size_t> out_set(it, unassigned_end);
				unassigned_end = it;

				handle const n = hull.insert(hull.end(), { t, { null, null, null, h }, true, std::move(out_set) });
				h->neigh[i] = n;
				face_queue.push(n);

				boundary_map[canonical({t[0], t[3], t[1]})] = {n, 2};
				boundary_map[canonical({t[0], t[2], t[3]})] = {n, 1};
				boundary_map[canonical({t[1], t[3], t[2]})] = {n, 0};
			}

			for (auto const & p : boundary_map)
			{
				p.second.first->neigh[p.second.second] = boundary_map.at(dual(p.first)).first;
			}

			++iteration;
		}

		std::vector<std::array<std::size_t, 4>> result;
		result.reserve(hull.size());

		for (auto const & f : hull)
			result.push_back(f.simplex);

		return result;
	}

	extern template std::vector<std::array<std::size_t, 4>> quickhull (std::vector<geom::point<float, 4ul>> const & points);
	extern template std::vector<std::array<std::size_t, 4>> quickhull (std::vector<geom::point<double, 4ul>> const & points);

}
