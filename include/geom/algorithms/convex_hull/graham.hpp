#pragma once

#include <geom/operations/orientation.hpp>
#include <geom/operations/compare/point.hpp>
#include <geom/algorithms/convex_hull/detail/wrap.hpp>

#include <vector>
#include <algorithm>
#include <type_traits>

namespace geom
{

	template <typename Iterator>
	Iterator in_place_graham_hull (Iterator begin, Iterator end)
	{
		std::iter_swap(std::min_element(begin, end), begin);

		auto comp = [begin](auto && p1, auto && p2)
		{
			return left(p1, p2, *begin);
		};

		std::sort(std::next(begin, 1), end, comp);

		auto hull_end = std::next(begin, 2);

		return detail::wrap(hull_end, end, begin, hull_end);
	}

	template <typename Iterator, typename OutputIterator>
	OutputIterator graham_hull (Iterator begin, Iterator end, OutputIterator out)
	{
		std::vector<typename std::remove_cv<typename std::iterator_traits<Iterator>::value_type>::type> points(begin, end);
		points.erase(in_place_graham_hull(points.begin(), points.end()), points.end());
		return std::copy(points.begin(), points.end(), out);
	}


}
