#pragma once

#include <geom/operations/orientation.hpp>
#include <geom/operations/compare/point.hpp>

#include <vector>
#include <algorithm>

namespace geom
{

	template <typename Iterator>
	Iterator in_place_merge_hull (Iterator begin, Iterator end, bool sorted = false)
	{
		auto distance = std::distance(begin, end);

		if (!sorted)
			std::sort(begin, end);

		if (distance <= 2)
			return end;

		auto middle = std::next(begin, distance / 2);

		auto hull_end_1 = in_place_merge_hull(begin, middle, true);
		auto hull_end_2 = in_place_merge_hull(middle, end, true);

		auto corner_1 = std::max_element(begin, hull_end_1);
		auto corner_2 = std::min_element(middle, hull_end_2);

		// find upper edge

		auto up_1 = corner_1;
		auto up_2 = corner_2;

		while (true)
		{
			bool b1 = false;
			bool b2 = false;

			while (left(*std::next(up_1), *up_1, *up_2))
			{
				b1 = true;
				++up_1;
			}

			while (left(*up_1, *up_2, *std::prev(up_2)))
			{
				b2 = true;
				--up_2;
			}

			if (!b1 && !b2)
				break;
		}

		// find lower edge

		auto low_1 = corner_1;
		auto low_2 = corner_2;

		while (true)
		{
			bool b1 = false;
			bool b2 = false;

			while (left(*std::prev(low_1), *low_1, *low_2))
			{
				b1 = true;
				--low_1;
			}

			while (left(*std::next(low_2), *low_2, *low_1))
			{
				b2 = true;
				++low_2;
			}

			if (!b1 && !b2)
				break;
		}

		// TODO
	}

	template <typename Iterator, typename OutputIterator>
	OutputIterator merge_hull (Iterator begin, Iterator end, OutputIterator out)
	{
		std::vector<typename std::iterator_traits<Iterator>::value_type> points(begin, end);
		points.erase(in_place_merge_hull(points.begin(), points.end()), points.end());
		return std::copy(points.begin(), points.end(), out);
	}


}
