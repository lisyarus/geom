#pragma once

#include <geom/operations/orientation.hpp>
#include <geom/operations/compare/point.hpp>
#include <geom/algorithms/convex_hull/detail/wrap.hpp>

#include <vector>
#include <algorithm>

namespace geom
{

	template <typename Iterator>
	Iterator in_place_andrew_hull (Iterator begin, Iterator end)
	{
		typedef typename std::iterator_traits<Iterator>::value_type point_type;

		if (std::distance(begin, end) < 4)
			return end;

		auto min_it = std::min_element(begin, end);
		std::iter_swap(begin, min_it);

		auto max_it = std::max_element(begin, end);
		std::iter_swap(std::next(begin), max_it);

		min_it = begin;
		max_it = std::next(begin);

		auto part = std::partition(std::next(begin, 2), end, [min_it, max_it](point_type const & p)
		{
			return !left(*min_it, *max_it, p);
		});

		--part;

		std::iter_swap(max_it, part);

		std::sort(std::next(begin), part);
		std::sort(std::next(part), end, std::greater<point_type>());

		// From now, the sequence looks like this:
		//   *begin - leftmost point
		//   [begin + 1, part) - lower points by ascending x-coordinate
		//   *part - rightmost point
		//   [part + 1, end) - upper points by descending x-coordinate

		auto hull_end = std::next(begin, 2);

		if (max_it != part)
		{
			hull_end = detail::wrap(hull_end, part, begin, hull_end);

			*hull_end++ = *part;
		}

		hull_end = detail::wrap(std::next(part), end, begin, hull_end);

		while (!left(*std::prev(hull_end, 2), *std::prev(hull_end), *min_it))
			--hull_end;

		return hull_end;
	}

	template <typename Iterator, typename OutputIterator>
	OutputIterator andrew_hull (Iterator begin, Iterator end, OutputIterator out)
	{
		std::vector<typename std::iterator_traits<Iterator>::value_type> points(begin, end);
		points.erase(in_place_andrew_hull(points.begin(), points.end()), points.end());
		return std::copy(points.begin(), points.end(), out);
	}


}
