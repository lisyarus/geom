#pragma once

#include <iterator>

namespace geom
{

	namespace detail
	{

		template <typename Iterator>
		Iterator wrap (Iterator begin, Iterator end, Iterator hull_begin, Iterator hull_end)
		{
			for (auto it = begin; it != end; ++it)
			{
				for (auto jt = std::prev(hull_end, 1); jt != hull_begin;)
				{
					--jt;
					if (left(*jt, *std::next(jt), *it))
					{
						jt = std::next(jt, 2);
						*jt = *it;
						hull_end = std::next(jt);
						break;
					}

					if (jt == hull_begin)
					{
						++jt;
						*jt = *it;
						hull_end = std::next(jt);
						break;
					}
				}
			}
			return hull_end;
		}

	}

}
