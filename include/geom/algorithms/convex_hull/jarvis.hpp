#pragma once

#include <geom/operations/orientation.hpp>
#include <geom/operations/compare/point.hpp>

#include <vector>
#include <algorithm>

namespace geom
{

	template <typename Iterator>
	Iterator in_place_jarvis_hull (Iterator begin, Iterator end)
	{
		typedef typename std::iterator_traits<Iterator>::reference reference;

		std::iter_swap(std::min_element(begin, end), begin);

		auto hull_end = std::next(begin);

		while (hull_end != end)
		{
			auto min_it = std::min_element(hull_end, end, [hull_end](reference p1, reference p2)
			{
				return left(*std::prev(hull_end), p1, p2);
			});

			if (std::prev(hull_end) == begin || left(*std::prev(hull_end), *min_it, *begin))
			{
				std::iter_swap(hull_end, min_it);
				++hull_end;
			}
			else
			{
				break;
			}
		}

		return hull_end;
	}

	template <typename Iterator, typename OutputIterator>
	OutputIterator jarvis_hull (Iterator begin, Iterator end, OutputIterator out)
	{
		std::vector<typename std::iterator_traits<Iterator>::value_type> points(begin, end);
		points.erase(in_place_jarvis_hull(points.begin(), points.end()), points.end());
		return std::copy(points.begin(), points.end(), out);
	}


}
