#pragma once

#include <geom/primitives/point.hpp>
#include <geom/operations/orientation.hpp>
#include <geom/operations/point.hpp>

#include <vector>
#include <list>
#include <queue>
#include <map>
#include <algorithm>

namespace geom
{

	template <typename T>
	std::vector<std::array<std::size_t, 3>> quickhull (std::vector<geom::point<T, 3ul>> const & points)
	{
		struct face;

		using handle = typename std::list<face>::iterator;
		using edge = std::array<std::size_t, 2>;
		using triangle = std::array<std::size_t, 3>;

		using point = geom::point<T, 3ul>;

		struct face
		{
			triangle tri;
			handle neigh[3];
			bool valid;
			std::vector<std::size_t> out;
		};

		std::list<face> hull;
		std::list<face> deleted;

		handle const null = hull.end();

		std::queue<handle> face_queue;

		{
			// lowest 'x'
			std::size_t i0 = std::min_element(points.begin(), points.end(), [](point const & p1, point const & p2){
				return p1[0] < p2[0];
			}) - points.begin();

			// furthest from i0
			std::size_t i1 = std::max_element(points.begin(), points.end(), [&](point const & p1, point const & p2){
				return geom::distance(points[i0], p1) < geom::distance(points[i0], p2);
			}) - points.begin();

			auto n01 = geom::normalized(points[i1] - points[i0]);
			auto dist01 = [&](point const & p){
				auto l = p - points[i0];
				return geom::norm_sqr(l) - (l * n01) * (l * n01);
			};

			// furthest from i0-i1 line
			std::size_t i2 = std::max_element(points.begin(), points.end(), [&](point const & p1, point const & p2){
				return dist01(p1) < dist01(p2);
			}) - points.begin();

			auto dist012 = [&](point const & p){
				return std::abs(geom::det(p, points[i0], points[i1], points[i2]));
			};

			// highest 'z'
			std::size_t i3 = std::max_element(points.begin(), points.end(), [&](point const & p1, point const & p2){
				return dist012(p1) < dist012(p2);
			}) - points.begin();

			if (geom::right(points[i0], points[i1], points[i2], points[i3]))
				std::swap(i2, i3);

			triangle t0 { i0, i1, i2 };
			triangle t1 { i0, i3, i1 };
			triangle t2 { i0, i2, i3 };
			triangle t3 { i1, i3, i2 };

			std::vector<std::size_t> out0, out1, out2, out3;

			for (std::size_t i = 0; i < points.size(); ++i)
			{
				if (i == i0) continue;
				if (i == i1) continue;
				if (i == i2) continue;
				if (i == i3) continue;

				if (geom::right(points[t0[0]], points[t0[1]], points[t0[2]], points[i]))
					out0.push_back(i);
				else if (geom::right(points[t1[0]], points[t1[1]], points[t1[2]], points[i]))
					out1.push_back(i);
				else if (geom::right(points[t2[0]], points[t2[1]], points[t2[2]], points[i]))
					out2.push_back(i);
				else if (geom::right(points[t3[0]], points[t3[1]], points[t3[2]], points[i]))
					out3.push_back(i);
			}

			auto it0 = hull.insert(hull.end(), {t0, {}, true, std::move(out0)});
			auto it1 = hull.insert(hull.end(), {t1, {}, true, std::move(out1)});
			auto it2 = hull.insert(hull.end(), {t2, {}, true, std::move(out2)});
			auto it3 = hull.insert(hull.end(), {t3, {}, true, std::move(out3)});

			it0->neigh[0] = it1;
			it0->neigh[1] = it3;
			it0->neigh[2] = it2;

			it1->neigh[0] = it2;
			it1->neigh[1] = it3;
			it1->neigh[2] = it0;

			it2->neigh[0] = it0;
			it2->neigh[1] = it3;
			it2->neigh[2] = it1;

			it3->neigh[0] = it1;
			it3->neigh[1] = it2;
			it3->neigh[2] = it0;

			face_queue.push(it0);
			face_queue.push(it1);
			face_queue.push(it2);
			face_queue.push(it3);
		}

		while (!face_queue.empty())
		{
			handle h = face_queue.front();
			face_queue.pop();

			if (!h->valid)
				continue;

			std::vector<std::size_t> const & out_set = h->out;

			if (out_set.empty())
				continue;

			triangle const t = h->tri;

			float dist = std::numeric_limits<float>::infinity();
			std::size_t max_i = out_set[0];

			for (auto i : out_set)
			{
				float const d = geom::det(points[t[0]], points[t[1]], points[t[2]], points[i]);

				if (d < dist)
				{
					dist = d;
					max_i = i;
				}
			}

			std::vector<handle> visible;

			std::queue<handle> visible_queue;
			visible_queue.push(h);
			h->valid = false;

			while (!visible_queue.empty())
			{
				auto h = visible_queue.front();
				visible_queue.pop();
				visible.push_back(h);

				for (std::size_t i : {0, 1, 2})
				{
					handle it = h->neigh[i];
					triangle const & t = it->tri;
					if (it->valid && geom::right(points[t[0]], points[t[1]], points[t[2]], points[max_i]))
					{
						it->valid = false;
						visible_queue.push(it);
					}
				}
			}

			std::vector<std::size_t> unassigned;
			std::vector<std::pair<handle, std::size_t>> boundary;

			for (auto h : visible)
			{
				auto & out = h->out;
				unassigned.insert(unassigned.end(), out.begin(), out.end());

				for (std::size_t i : {0, 1, 2})
				{
					handle n = h->neigh[i];
					if (n->valid)
					{
						bool found = false;

						for (std::size_t j : {0, 1, 2})
						{
							if (n->neigh[j] == h)
							{
								boundary.push_back({n, j});
								found = true;
								break;
							}
						}
					}
				}

				deleted.splice(deleted.end(), hull, h);
			}


			auto unassigned_end = std::prev(unassigned.end());

			std::iter_swap(std::find(unassigned.begin(), unassigned.end(), max_i), unassigned_end);

			std::map<edge, std::pair<handle, std::size_t>> edge_map;

			for (auto p : boundary)
			{
				handle h = p.first;
				std::size_t i = p.second;
				triangle const t { h->tri[(i+1)%3], h->tri[i], max_i };

				auto it = std::partition(unassigned.begin(), unassigned_end, [&](std::size_t i){
					return geom::left(points[t[0]], points[t[1]], points[t[2]], points[i]);
				});

				std::vector<std::size_t> out_set(it, unassigned_end);
				unassigned_end = it;

				hull.push_back({ t, { h, null, null }, true, std::move(out_set) });
				handle n = std::prev(hull.end());
				h->neigh[i] = n;
				face_queue.push(n);

				edge_map[{t[1], t[2]}] = {n, 1};
				edge_map[{t[2], t[0]}] = {n, 2};
			}

			for (auto const & p : edge_map)
			{
				p.second.first->neigh[p.second.second] = edge_map.at({p.first[1], p.first[0]}).first;
			}
		}

		std::vector<std::array<std::size_t, 3>> result;
		result.reserve(hull.size());

		for (auto const & f : hull)
			result.push_back(f.tri);

		return result;
	}

	extern template std::vector<std::array<std::size_t, 3>> quickhull (std::vector<geom::point<float, 3ul>> const & points);
	extern template std::vector<std::array<std::size_t, 3>> quickhull (std::vector<geom::point<double, 3ul>> const & points);

}
