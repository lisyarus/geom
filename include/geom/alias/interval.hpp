#pragma once

#include <geom/primitives/interval.hpp>
#include <geom/alias/helpers.hpp>

namespace geom
{

	_GEOM_GEN_ALIAS_T(interval, interval_)

}
