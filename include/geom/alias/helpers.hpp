#pragma once

#include <cstdint>
#include <complex>

#ifdef GEOM_QUADRUPLE
	#include <boost/multiprecision/float128.hpp>

	#define _GEOM_GEN_ALIAS_T(base, name) \
		typedef base<std::int8_t> name##b; \
		typedef base<std::int16_t> name##s; \
		typedef base<std::int32_t> name##i; \
		typedef base<std::uint8_t> name##ub; \
		typedef base<std::uint16_t> name##us; \
		typedef base<std::uint32_t> name##ui; \
		typedef base<std::uint64_t> name##ul; \
		typedef base<float> name##f; \
		typedef base<double> name##d; \
		typedef base<long double> name##ld; \
		typedef base<boost::multiprecision::float128> name##q; \
		typedef base<std::complex<float>> name##cf; \
		typedef base<std::complex<double>> name##cd; \
		typedef base<std::complex<long double>> name##cld; \
		typedef base<std::complex<boost::multiprecision::float128>> name##cq; \


#else

	#define _GEOM_GEN_ALIAS_T(base, name) \
		typedef base<std::int8_t> name##b; \
		typedef base<std::int16_t> name##s; \
		typedef base<std::int32_t> name##i; \
		typedef base<std::uint8_t> name##ub; \
		typedef base<std::uint16_t> name##us; \
		typedef base<std::uint32_t> name##ui; \
		typedef base<std::uint64_t> name##ul; \
		typedef base<float> name##f; \
		typedef base<double> name##d; \
		typedef base<long double> name##ld; \
		typedef base<std::complex<float>> name##cf; \
		typedef base<std::complex<double>> name##cd; \
		typedef base<std::complex<long double>> name##cld; \


#endif

#define _GEOM_GEN_ALIAS_D(name) \
	template <typename T> \
		using name##_1 = name<T, 1>; \
	template <typename T> \
		using name##_2 = name<T, 2>; \
	template <typename T> \
		using name##_3 = name<T, 3>; \
	template <typename T> \
		using name##_4 = name<T, 4>; \
	_GEOM_GEN_ALIAS_T(name##_1, name##_1) \
	_GEOM_GEN_ALIAS_T(name##_2, name##_2) \
	_GEOM_GEN_ALIAS_T(name##_3, name##_3) \
	_GEOM_GEN_ALIAS_T(name##_4, name##_4) \

