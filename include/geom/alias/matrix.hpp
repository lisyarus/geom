#pragma once

#include <geom/primitives/matrix.hpp>
#include <geom/alias/helpers.hpp>

namespace geom
{

	template <typename T>
	using matrix_1x1 = matrix<T, 1, 1>;

	template <typename T>
	using matrix_2x1 = matrix<T, 2, 1>;

	template <typename T>
	using matrix_3x1 = matrix<T, 3, 1>;

	template <typename T>
	using matrix_4x1 = matrix<T, 4, 1>;

	template <typename T>
	using matrix_1x2 = matrix<T, 1, 2>;

	template <typename T>
	using matrix_2x2 = matrix<T, 2, 2>;

	template <typename T>
	using matrix_3x2 = matrix<T, 3, 2>;

	template <typename T>
	using matrix_4x2 = matrix<T, 4, 2>;

	template <typename T>
	using matrix_1x3 = matrix<T, 1, 3>;

	template <typename T>
	using matrix_2x3 = matrix<T, 2, 3>;

	template <typename T>
	using matrix_3x3 = matrix<T, 3, 3>;

	template <typename T>
	using matrix_4x3 = matrix<T, 4, 3>;

	template <typename T>
	using matrix_1x4 = matrix<T, 1, 4>;

	template <typename T>
	using matrix_2x4 = matrix<T, 2, 4>;

	template <typename T>
	using matrix_3x4 = matrix<T, 3, 4>;

	template <typename T>
	using matrix_4x4 = matrix<T, 4, 4>;

	_GEOM_GEN_ALIAS_T(matrix_1x1, matrix_1x1)
	_GEOM_GEN_ALIAS_T(matrix_2x1, matrix_2x1)
	_GEOM_GEN_ALIAS_T(matrix_3x1, matrix_3x1)
	_GEOM_GEN_ALIAS_T(matrix_4x1, matrix_4x1)
	_GEOM_GEN_ALIAS_T(matrix_1x2, matrix_1x2)
	_GEOM_GEN_ALIAS_T(matrix_2x2, matrix_2x2)
	_GEOM_GEN_ALIAS_T(matrix_3x2, matrix_3x2)
	_GEOM_GEN_ALIAS_T(matrix_4x2, matrix_4x2)
	_GEOM_GEN_ALIAS_T(matrix_1x3, matrix_1x3)
	_GEOM_GEN_ALIAS_T(matrix_2x3, matrix_2x3)
	_GEOM_GEN_ALIAS_T(matrix_3x3, matrix_3x3)
	_GEOM_GEN_ALIAS_T(matrix_4x3, matrix_4x3)
	_GEOM_GEN_ALIAS_T(matrix_1x4, matrix_1x4)
	_GEOM_GEN_ALIAS_T(matrix_2x4, matrix_2x4)
	_GEOM_GEN_ALIAS_T(matrix_3x4, matrix_3x4)
	_GEOM_GEN_ALIAS_T(matrix_4x4, matrix_4x4)

}
