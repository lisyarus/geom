#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/operations/vector.hpp>

#include <random>

namespace geom
{

	namespace random
	{

		template <typename V>
		struct uniform_sphere_vector_distribution
		{
			typedef V vector_type;

			typedef typename V::value_type coordinate_type;
			static const auto dimension = V::dimension;

			typedef std::normal_distribution<coordinate_type> distribution_type;

			uniform_sphere_vector_distribution (coordinate_type radius = coordinate_type(1))
				: radius_(radius)
			{ }

			template <typename RNG>
			V operator () (RNG && rng)
			{
				V result;
				distribution_type d;
				for (std::size_t i = 0; i < dimension; ++i)
					result[i] = d(rng);
				return radius_ * normalized(result);
			}

		private:
			coordinate_type radius_;
		};

	}

}
