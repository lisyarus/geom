#pragma once

#include <geom/primitives/point.hpp>
#include <geom/primitives/rectangle.hpp>

#include <random>

namespace geom
{

	namespace random
	{

		template <typename P>
		struct uniform_rectangle_point_distribution
		{
			typedef P point_type;

			typedef typename P::value_type coordinate_type;
			static const auto dimension = P::dimension;

			typedef rectangle<coordinate_type, dimension> rectangle_type;

			typedef typename std::conditional<
				std::is_integral<coordinate_type>::value,
				std::uniform_int_distribution<coordinate_type>,
				std::uniform_real_distribution<coordinate_type>>::type
					distribution_type;

			uniform_rectangle_point_distribution (rectangle_type rect)
				: rect_(rect)
			{ }

			template <typename RNG>
			P operator () (RNG && rng)
			{
				P result;
				for (std::size_t i = 0; i < dimension; ++i)
					result[i] = distribution_type(rect_[i].inf, rect_[i].sup)(rng);
				return result;
			}

		private:
			rectangle_type rect_;
		};

		template <typename P>
		struct uniform_disk_point_distribution
		{
			typedef P point_type;

			typedef typename P::value_type coordinate_type;
			static const auto dimension = P::dimension;

			typedef vector<coordinate_type, dimension> vector_type;

			static_assert(dimension == 2, "dimension == 2");

			typedef typename std::conditional<
				std::is_integral<coordinate_type>::value,
				std::uniform_int_distribution<coordinate_type>,
				std::uniform_real_distribution<coordinate_type>>::type
					distribution_type;

			uniform_disk_point_distribution (point_type center, coordinate_type radius)
				: center_(center)
				, radius_(radius)
			{ }

			template <typename RNG>
			P operator () (RNG && rng)
			{
				auto phi = distribution_type(0, 2 * M_PI)(rng);
				auto r = std::sqrt(distribution_type(0, radius_ * radius_)(rng));
				return center_ + vector_type{std::cos(phi) * r, std::sin(phi) * r};
			}

		private:
			point_type center_;
			coordinate_type radius_;
		};

	}

}
