#pragma once

#include <cstddef>
#include <utility>

namespace geom
{

	template <typename Array>
	struct is_array
	{
	private:

		typedef char true_t;

		struct false_t { char _d[2]; };

		template <typename A>
		static true_t check (typename A::value_type *);

		template <typename A>
		static false_t check (...);

	public:
		static constexpr bool value = sizeof(check<Array>(0)) == sizeof(true_t);
	};

}

#define _GEOM_DECLARE_ARRAY(T, D) \
		static constexpr std::size_t dimension = D; \
\
		using value_type = T; \
		using reference = T &; \
		using pointer = T *; \
		using const_reference = T const &; \
		using const_pointer = T const *; \
\
		using size_type = std::size_t; \
		using difference_type = std::ptrdiff_t; \
\
		using iterator = pointer; \
		using const_iterator = const_pointer; \
\
		value_type array[D]; \
\
		reference operator [] (size_type i) noexcept \
		{ return array[i]; } \
\
		const_reference constexpr operator [] (size_type i) const noexcept \
		{ return array[i]; } \
\
		constexpr size_type size ( ) const noexcept \
		{ return D; } \
\
		pointer data ( ) noexcept \
		{ return array; } \
\
		const_pointer data ( ) const noexcept \
		{ return array; } \
\
		iterator begin ( ) noexcept \
		{ return array; } \
\
		iterator end ( ) noexcept \
		{ return begin() + size(); } \
\
		const_iterator begin ( ) const noexcept \
		{ return array; } \
\
		const_iterator end ( ) const noexcept \
		{ return begin() + size(); } \

