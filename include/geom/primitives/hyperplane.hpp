#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/operations/vector.hpp>

namespace geom
{

	// Technically, an oriented affine subspace of codimention 1
	// Implemented via a homogeneous 1-form
	template <typename T, std::size_t D>
	struct hyperplane
	{
		geom::vector<T, D + 1> f;

		// result = 0: point is contained in the subspace
		// result > 0: point is contained in the interior of the half-plane
		T operator() (point<T, D> const & p) const;

		// the same as for points, without considering translations
		T operator() (vector<T, D> const & p) const;

		hyperplane & operator += (vector<T, D> const & v);
		hyperplane & operator -= (vector<T, D> const & v);
	};

	template <typename T, std::size_t D>
	T hyperplane<T, D>::operator() (point<T, D> const & p) const
	{
		return f * geom::homogeneous(p);
	}

	template <typename T, std::size_t D>
	T hyperplane<T, D>::operator() (vector<T, D> const & v) const
	{
		return f * geom::homogeneous(v);
	}

	template <typename T, std::size_t D>
	hyperplane<T, D> & hyperplane<T, D>::operator += (vector<T, D> const & v)
	{
		f[D] -= f * geom::homogeneous(v);
		return *this;
	}

	template <typename T, std::size_t D>
	hyperplane<T, D> & hyperplane<T, D>::operator -= (vector<T, D> const & v)
	{
		f[D] += f * geom::homogeneous(v);
		return *this;
	}

	template <typename T, std::size_t D>
	hyperplane<T, D> operator + (hyperplane<T, D> const & h, vector<T, D> const & v)
	{
		hyperplane<T, D> hh = h;
		hh += v;
		return hh;
	}

	template <typename T, std::size_t D>
	hyperplane<T, D> operator + (vector<T, D> const & v, hyperplane<T, D> const & h)
	{
		hyperplane<T, D> hh = h;
		hh += v;
		return hh;
	}

	template <typename T, std::size_t D>
	hyperplane<T, D> operator - (hyperplane<T, D> const & h, vector<T, D> const & v)
	{
		hyperplane<T, D> hh = h;
		hh -= v;
		return hh;
	}

}
