#pragma once

#include <geom/array.hpp>
#include <geom/functional.hpp>
#include <geom/primitives/vector.hpp>

#include <functional>

namespace geom
{

	struct point_tag;

	template <typename T, std::size_t D>
	struct point
	{
		_GEOM_DECLARE_ARRAY(T, D)

		using vector_type = vector<T, D>;

		static point zero ( );

		point & operator += (vector<T, D> const & v);
		point & operator -= (vector<T, D> const & v);
	};

	template <typename T, std::size_t D>
	point<T, D> point<T, D>::zero ( )
	{
		point<T, D> p;
		for (auto & x : p)
			x = T(0);
		return p;
	}

	template <typename ... Ts>
	auto make_point (Ts && ... ts)
	{
		using T = std::common_type_t<Ts...>;
		return point<T, sizeof...(Ts)>{ std::forward<Ts>(ts)... };
	}

	// point cast

	template <typename T, typename H, std::size_t D>
	point<T, D> cast (point<H, D> const & p)
	{
		return functional::map<point<T, D>>(functional::cast<T, H>(), p);
	}

	// point + vector
	template <typename T, std::size_t D>
	point<T, D> operator + (point<T, D> const & p, vector<T, D> const & v)
	{
		return functional::map<point<T, D>>(functional::plus(), p, v);
	}

	// point - vector
	template <typename T, std::size_t D>
	point<T, D> operator - (point<T, D> const & p, vector<T, D> const & v)
	{
		return functional::map<point<T, D>>(functional::minus(), p, v);
	}

	// vector + point
	template <typename T, std::size_t D>
	point<T, D> operator + (vector<T, D> const & v, point<T, D> const & p)
	{
		return functional::map<point<T, D>>(functional::plus(), v, p);
	}

	// point - point
	template <typename T, std::size_t D>
	vector<T, D> operator - (point<T, D> const & p1, point<T, D> const & p2)
	{
		return functional::map<vector<T, D>>(functional::minus(), p1, p2);
	}

	// point += vector
	template <typename T, std::size_t D>
	point<T, D> & point<T, D>::operator += (vector<T, D> const & v)
	{
		return *this = *this + v;
	}

	// point -= vector
	template <typename T, std::size_t D>
	point<T, D> & point<T, D>::operator -= (vector<T, D> const & v)
	{
		return *this = *this - v;
	}
}
