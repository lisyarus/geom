#pragma once

#include <geom/primitives/vector.hpp>
#include <geom/operations/vector.hpp>

namespace geom
{

	template <typename T, std::size_t R, std::size_t C>
	struct matrix
	{
		using row_type = vector<T, C>;

		_GEOM_DECLARE_ARRAY(row_type, R)

		static matrix zero ( );
		static matrix identity ( );

		matrix & operator *= (T s);
		matrix & operator /= (T s);

		matrix & operator += (matrix const & m);
		matrix & operator -= (matrix const & m);
	};

	template <typename T, typename H, std::size_t R, std::size_t C>
	matrix<T, R, C> cast (matrix<H, R, C> const & m)
	{
		matrix<T, R, C> n;

		for (std::size_t r = 0; r < R; ++r)
		{
			for (std::size_t c = 0; c < C; ++c)
			{
				n[r][c] = static_cast<T>(m[r][c]);
			}
		}

		return n;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> matrix<T, R, C>::zero ( )
	{
		matrix<T, R, C> r;
		for (auto & v : r)
			for (auto & x : v)
				x = T(0);
		return r;
	}

	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> matrix<T, R, C>::identity ( )
	{
		matrix<T, R, C> r = zero();
		for (std::size_t i = 0; i < std::min(R, C); ++i)
			r[i][i] = T(1);
		return r;
	}

	// + matrix
	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator + (matrix<T, R, C> const & m)
	{
		return m;
	}

	// - matrix
	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator - (matrix<T, R, C> m)
	{
		return functional::map<matrix<T, R, C>>(functional::negate(), m);
	}

	// matrix * scalar
	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator * (matrix<T, R, C> m, T s)
	{
		return functional::map<matrix<T, R, C>>(functional::multiplies2(s), m);
	}

	// scalar * matrix
	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator * (T s, matrix<T, R, C> m)
	{
		return functional::map<matrix<T, R, C>>(functional::multiplies1(s), m);
	}

	// matrix * scalar
	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator / (matrix<T, R, C> m, T s)
	{
		return functional::map<matrix<T, R, C>>(functional::divides2(s), m);
	}

	// matrix + matrix
	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator + (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return functional::map<matrix<T, R, C>>(functional::plus(), m1, m2);
	}

	// matrix - matrix
	template <typename T, std::size_t R, std::size_t C>
	matrix<T, R, C> operator - (matrix<T, R, C> const & m1, matrix<T, R, C> const & m2)
	{
		return functional::map<matrix<T, R, C>>(functional::minus(), m1, m2);
	}

	// matrix * vector
	template <typename T, std::size_t R, std::size_t C>
	vector<T, R> operator * (matrix<T, R, C> const & m, vector<T, C> const & v)
	{
		return functional::map<vector<T, R>>(functional::multiplies2(v), m);
	}

	// matrix * matrix
	template <typename T, std::size_t R, std::size_t N, std::size_t C>
	matrix<T, R, C> operator * (matrix<T, R, N> const & m1, matrix<T, N, C> const & m2)
	{
		// I failed to come up with a more elegant implementation
		matrix<T, R, C> m = matrix<T, R, C>::zero();

		for (std::size_t r = 0; r < R; ++r)
		{
			for (std::size_t c = 0; c < C; ++c)
			{
				for (std::size_t n = 0; n < N; ++n)
				{
					m[r][c] += m1[r][n] * m2[n][c];
				}
			}
		}

		return m;
	}


}
