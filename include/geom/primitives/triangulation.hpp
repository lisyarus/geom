#pragma once

#include <geom/primitives/simplex.hpp>

#include <vector>
#include <array>

namespace geom
{

	// Abstract triangulation only cares about points' identity, not geometry
	template <std::size_t N, typename Index = std::size_t>
	struct abstract_triangulation
	{
		// Contains indices; an N-simplex contains N+1 points
		// Note: NOT the same as geom::simplex
		using simplex = std::array<Index, N+1>;

		std::vector<simplex> simplices;
	};

	template <typename P, std::size_t N, typename Index = std::size_t>
	struct triangulation
		: abstract_triangulation<N, Index>
	{
		std::vector<P> vertices;

		geom::simplex<P, N> get (std::array<Index, N+1> const & s) const
		{
			geom::simplex<P, N> result;
			for (Index i = 0; i <= N; ++i)
				result[i] = vertices[s[i]];
			return result;
		}

		geom::simplex<P, N> get (std::size_t simplex_index) const
		{
			return get(this->simplices[simplex_index]);
		}
	};

	struct merge_tag_t { };
	static constexpr merge_tag_t merge_tag;

	template <typename Index = std::size_t, typename P, std::size_t N>
	triangulation<P, N, Index> triangulate (simplex<P, N> const & s)
	{
		triangulation<P, N, Index> result;
		std::array<Index, N + 1> si;

		for (std::size_t i = 0; i <= N; ++i)
		{
			result.vertices.push_back(s[i]);
			si[i] = i;
		}

		result.simplices.push_back(si);

		return result;
	}

}
