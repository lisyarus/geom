#pragma once

#include <geom/array.hpp>
#include <geom/functional.hpp>

#include <functional>
#include <type_traits>

namespace geom
{

	template <typename T, std::size_t D>
	struct vector
	{
		_GEOM_DECLARE_ARRAY(T, D)

		static vector zero ( );

		vector & operator += (vector const & v);
		vector & operator -= (vector const & v);

		vector & operator *= (T s);
		vector & operator /= (T s);
	};

	template <typename T, std::size_t D>
	vector<T, D> vector<T, D>::zero ( )
	{
		vector<T, D> v;
		for (auto & x : v)
			x = T(0);
		return v;
	}

	template <typename ... Ts>
	auto make_vector (Ts && ... ts)
	{
		using T = std::common_type_t<Ts...>;
		return vector<T, sizeof...(Ts)>{ std::forward<Ts>(ts)... };
	}

	// vector cast

	template <typename T, typename H, std::size_t D>
	vector<T, D> cast (vector<H, D> const & v)
	{
		return functional::map<vector<T, D>>(functional::cast<T, H>(), v);
	}

	// + vector
	template <typename T, std::size_t D>
	vector<T, D> operator + (vector<T, D> const & v)
	{
		return v;
	}

	// - vector
	template <typename T, std::size_t D>
	vector<T, D> operator - (vector<T, D> const & v)
	{
		return functional::map<vector<T, D>>(functional::negate(), v);
	}

	// vector * scalar
	template <typename T, std::size_t D>
	vector<T, D> operator * (vector<T, D> const & v, T s)
	{
		return functional::map<vector<T, D>>(functional::multiplies2(s), v);
	}

	// scalar * vector
	template <typename T, std::size_t D>
	vector<T, D> operator * (T s, vector<T, D> const & v)
	{
		return functional::map<vector<T, D>>(functional::multiplies1(s), v);
	}

	// vector / scalar
	template <typename T, std::size_t D>
	vector<T, D> operator / (vector<T, D> const & v, T s)
	{
		return functional::map<vector<T, D>>(functional::divides2(s), v);
	}

	// vector + vector
	template <typename T, std::size_t D>
	vector<T, D> operator + (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return functional::map<vector<T, D>>(functional::plus(), v1, v2);
	}

	// vector - vector
	template <typename T, std::size_t D>
	vector<T, D> operator - (vector<T, D> const & v1, vector<T, D> const & v2)
	{
		return functional::map<vector<T, D>>(functional::minus(), v1, v2);
	}

	// vector += vector

	template <typename T, std::size_t D>
	vector<T, D> & vector<T, D>::operator += (vector<T, D> const & v)
	{
		return *this = *this + v;
	}

	// vector -= vector

	template <typename T, std::size_t D>
	vector<T, D> & vector<T, D>::operator -= (vector<T, D> const & v)
	{
		return *this = *this - v;
	}

	// vector *= scalar

	template <typename T, std::size_t D>
	vector<T, D> & vector<T, D>::operator *= (T s)
	{
		return *this = *this * s;
	}

	// vector /= scalar

	template <typename T, std::size_t D>
	vector<T, D> & vector<T, D>::operator /= (T s)
	{
		return *this = *this / s;
	}
}
