#pragma once

#include <geom/primitives/vector.hpp>

#include <array>

namespace geom
{

	// Yep, an abstract simplex is determined by
	// a single constant N, hence an empty class
	template <std::size_t N>
	struct abstract_simplex
	{ };

	template <typename P, std::size_t N>
	struct simplex
	{
		// An N-simplex contains N+1 points
		_GEOM_DECLARE_ARRAY(P, N+1)

		using point_type = P;
		using vector_type = typename P::vector_type;

		simplex & operator += (vector_type const & v);
		simplex & operator -= (vector_type const & v);
	};

	template <typename P>
	using segment = simplex<P, 1>;

	template <typename P>
	using triangle = simplex<P, 2>;

	template <typename P>
	using tetrahedron = simplex<P, 3>;

	template <typename P, std::size_t N>
	simplex<P, N> operator + (simplex<P, N> s, typename simplex<P, N>::vector_type const & v)
	{
		for (auto & p : s)
			p += v;
		return s;
	}

	template <typename P, std::size_t N>
	simplex<P, N> operator - (simplex<P, N> s, typename simplex<P, N>::vector_type const & v)
	{
		for (auto & p : s)
			p -= v;
		return s;
	}

	template <typename P, std::size_t N>
	simplex<P, N> & simplex<P, N>::operator += (vector_type const & v)
	{
		return *this = *this + v;
	}

	template <typename P, std::size_t N>
	simplex<P, N> & simplex<P, N>::operator -= (vector_type const & v)
	{
		return *this = *this - v;
	}

}
