#pragma once

#include <limits>
#include <cmath>
#include <type_traits>

namespace geom
{

	namespace detail
	{
		template <typename T, bool FloatingPoint>
		struct limits_helper
		{
			static constexpr T max ( )
			{
				return std::numeric_limits<T>::max();
			}

			static constexpr T min ( )
			{
				return std::numeric_limits<T>::min();
			}

			static constexpr bool is_infinite (T inf, T sup)
			{
				return false;
			}
		};

		template <typename T>
		struct limits_helper<T, true>
		{
			static constexpr T max ( )
			{
				return std::numeric_limits<T>::infinity();
			}

			static constexpr T min ( )
			{
				return -std::numeric_limits<T>::infinity();
			}

			static constexpr bool is_infinite (T inf, T sup)
			{
				return
					(inf == min() && sup > inf) ||
					(inf < sup && sup == max());
			}
		};

		template <typename T>
		struct limits
			: detail::limits_helper<T, std::is_floating_point<T>::value>
		{ };
	}

	template <typename T>
	struct interval
	{
		using value_type = T;

		using limits_type = detail::limits<T>;

		value_type inf, sup;

		constexpr interval ( )
			: inf(limits_type::max())
			, sup(limits_type::min())
		{ }

		explicit constexpr interval (value_type v)
			: inf(v)
			, sup(v)
		{ }

		constexpr interval (value_type inf, value_type sup)
			: inf(inf)
			, sup(sup)
		{ }

		static constexpr interval full ( )
		{
			return interval(limits_type::min(), limits_type::max());
		}

		bool constexpr empty ( ) const
		{
			return inf > sup;
		}

		bool constexpr infinite ( ) const
		{
			return limits_type::is_infinite(inf, sup);
		}

		value_type constexpr length ( ) const
		{
			return empty() ? T(0) : sup - inf;
		}

		value_type constexpr center ( ) const
		{
			return (inf + sup) / 2;
		}

		template <typename H>
		auto operator() (H h) const
		{
			return inf + (sup - inf) * h;
		}

		interval & operator &= (interval const &);
		interval & operator |= (interval const &);

		interval & operator += (T s);
		interval & operator -= (T s);
		interval & operator *= (T s);
		interval & operator /= (T s);
	};

	template <typename T1, typename T2>
	auto make_interval (T1 && t1, T2 && t2)
	{
		using T = std::common_type_t<T1, T2>;
		return interval<T>{ std::forward<T1>(t1), std::forward<T2>(t2) };
	}

	template <typename T, typename H>
	interval<T> cast (interval<H> const & i)
	{
		return interval<T>{ static_cast<T>(i.inf), static_cast<T>(i.sup) };
	}

	template <typename T>
	interval<T> operator & (interval<T> const & i1, interval<T> const & i2)
	{
		return interval<T>(std::max(i1.inf, i2.inf), std::min(i1.sup, i2.sup));
	}

	template <typename T>
	interval<T> operator | (interval<T> const & i1, interval<T> const & i2)
	{
		return interval<T>(std::min(i1.inf, i2.inf), std::max(i1.sup, i2.sup));
	}

	template <typename T>
	interval<T> operator + (interval<T> const & i, T x)
	{
		return {i.inf + x, i.sup + x};
	}

	template <typename T>
	interval<T> operator + (T x, interval<T> const & i)
	{
		return {i.inf + x, i.sup + x};
	}

	template <typename T>
	interval<T> operator - (interval<T> const & i, T x)
	{
		return {i.inf - x, i.sup - x};
	}

	template <typename T>
	interval<T> operator * (interval<T> const & i, T x)
	{
		return {i.inf * x, i.sup * x};
	}

	template <typename T>
	interval<T> operator * (T x, interval<T> const & i)
	{
		return {i.inf * x, i.sup * x};
	}

	template <typename T>
	interval<T> operator / (interval<T> const & i, T x)
	{
		return {i.inf / x, i.sup / x};
	}

	template <typename T>
	interval<T> & interval<T>::operator &= (interval<T> const & i)
	{
		return *this = *this & i;
	}

	template <typename T>
	interval<T> & interval<T>::operator |= (interval<T> const & i)
	{
		return *this = *this | i;
	}

	template <typename T>
	interval<T> & interval<T>::operator += (T x)
	{
		return *this = *this + x;
	}

	template <typename T>
	interval<T> & interval<T>::operator -= (T x)
	{
		return *this = *this - x;
	}

	template <typename T>
	interval<T> & interval<T>::operator *= (T x)
	{
		return *this = *this * x;
	}

	template <typename T>
	interval<T> & interval<T>::operator /= (T x)
	{
		return *this = *this / x;
	}

}
