#pragma once

#include <geom/array.hpp>
#include <geom/functional.hpp>
#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/interval.hpp>

#include <functional>

namespace geom
{

	template <typename T, std::size_t D>
	struct rectangle
	{
		_GEOM_DECLARE_ARRAY(interval<T>, D)

		using point_type = point<T, D>;

		static rectangle full ( )
		{
			rectangle result;
			for (auto & i : result)
				i = interval<T>::full();
			return result;
		}

		static rectangle singular (point_type const & p)
		{
			rectangle result;
			for (std::size_t i = 0; i < D; ++i)
				result[i] = interval<T>(p[i]);
			return result;
		}

		bool empty ( ) const
		{
			return functional::fold([](bool b, interval<T> const & i){ return b | i.empty(); }, *this, false);
		}

		bool infinite ( ) const
		{
			return functional::fold([](bool b, interval<T> const & i){ return b | i.infinite(); }, *this, false);
		}

		T volume ( ) const
		{
			return functional::fold([](T s, interval<T> const & i){ return s * i.length(); }, *this, T(1));
		}

		vector<T, D> lengths ( ) const
		{
			vector<T, D> result;
			for (std::size_t i = 0; i < D; ++i)
				result[i] = array[i].length();
			return result;
		}

		point_type center ( ) const
		{
			point_type p;
			for (std::size_t i = 0; i < D; ++i)
				p[i] = array[i].center();
			return p;
		}

		template <typename H>
		auto operator() (vector<H, D> const & h) const
		{
			using scalar_type = decltype(std::declval<H>() * std::declval<T>());
			point<scalar_type, D> r;
			for (std::size_t i = 0; i < D; ++i)
				r[i] = array[i](h[i]);
			return r;
		}

		template <typename ... Hs>
		auto operator() (Hs const & ... hs) const
		{
			return (*this)(make_vector(hs...));
		}

		rectangle & operator |= (point_type const & p);
		rectangle & operator &= (point_type const & p);
		rectangle & operator |= (rectangle const & r);
		rectangle & operator &= (rectangle const & r);

		rectangle & operator += (vector<T, D> const & v);
		rectangle & operator -= (vector<T, D> const & v);
	};

	// rectangle cast

	template <typename T, typename H, std::size_t D>
	rectangle<T, D> cast (rectangle<H, D> const & r)
	{
		auto fn = [](interval<H> const & i){ return cast<T>(i); };
		return functional::map<rectangle<T, D>>(fn, r);
	}

	// rectangle | rectangle
	template <typename T, std::size_t D>
	rectangle<T, D> operator | (rectangle<T, D> const & r1, rectangle<T, D> const & r2)
	{
		return functional::map<rectangle<T, D>>(functional::bit_or(), r1, r2);
	}

	// rectangle & rectangle
	template <typename T, std::size_t D>
	rectangle<T, D> operator & (rectangle<T, D> const & r1, rectangle<T, D> const & r2)
	{
		return functional::map<rectangle<T, D>>(functional::bit_and(), r1, r2);
	}

	// rectangle | point
	template <typename T, std::size_t D>
	rectangle<T, D> operator | (rectangle<T, D> const & r, point<T, D> const & p)
	{
		return r | rectangle<T, D>::singular(p);
	}

	// point | rectangle
	template <typename T, std::size_t D>
	rectangle<T, D> operator | (point<T, D> const & p, rectangle<T, D> const & r)
	{
		return rectangle<T, D>::singular(p) | r;
	}

	// rectangle & point
	template <typename T, std::size_t D>
	rectangle<T, D> operator & (rectangle<T, D> const & r, point<T, D> const & p)
	{
		return r & rectangle<T, D>::singular(p);
	}

	// point & rectangle
	template <typename T, std::size_t D>
	rectangle<T, D> operator & (point<T, D> const & p, rectangle<T, D> const & r)
	{
		return rectangle<T, D>::singular(p) & r;
	}

	template <typename T, std::size_t D>
	rectangle<T, D> & rectangle<T, D>::operator |= (point_type const & p)
	{
		return *this = *this | singular(p);
	}

	template <typename T, std::size_t D>
	rectangle<T, D> & rectangle<T, D>::operator &= (point_type const & p)
	{
		return *this = *this & singular(p);
	}

	template <typename T, std::size_t D>
	rectangle<T, D> & rectangle<T, D>::operator |= (rectangle const & r)
	{
		return *this = *this | r;
	}

	template <typename T, std::size_t D>
	rectangle<T, D> & rectangle<T, D>::operator &= (rectangle const & r)
	{
		return *this = *this & r;
	}

	// rectangle + vector
	template <typename T, std::size_t D>
	rectangle<T, D> operator + (rectangle<T, D> const & r, vector<T, D> const & v)
	{
		return functional::map<rectangle<T, D>>(functional::plus(), r, v);
	}

	// vector + rectangle
	template <typename T, std::size_t D>
	rectangle<T, D> operator + (vector<T, D> const & v, rectangle<T, D> const & r)
	{
		return functional::map<rectangle<T, D>>(functional::plus(), v, r);
	}

	// rectangle - vector
	template <typename T, std::size_t D>
	rectangle<T, D> operator - (rectangle<T, D> const & r, vector<T, D> const & v)
	{
		return functional::map<rectangle<T, D>>(functional::minus(), r, v);
	}

	// rectangle += vector
	template <typename T, std::size_t D>
	rectangle<T, D> & rectangle<T, D>::operator += (vector<T, D> const & v)
	{
		return *this = *this + v;
	}

	// rectangle -= vector
	template <typename T, std::size_t D>
	rectangle<T, D> & rectangle<T, D>::operator -= (vector<T, D> const & v)
	{
		return *this = *this - v;
	}

}
