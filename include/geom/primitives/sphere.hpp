#pragma once

#include <geom/primitives/point.hpp>
#include <geom/primitives/triangulation.hpp>
#include <geom/utility.hpp>

namespace geom
{

	template <typename T, std::size_t N>
	struct sphere
	{
		geom::point<T, N + 1> origin;
		T radius;

		sphere (geom::point<T, N + 1> origin = {T(), T()}, T radius = T(1))
			: origin(origin)
			, radius(radius)
		{ }
	};

	template <typename Index = std::size_t, typename T>
	triangulation<geom::point<T, 2>, 1, Index> triangulate (sphere<T, 1> const & s, std::size_t quality = 32)
	{
		triangulation<geom::point<T, 2>, 1, Index> result;
		result.vertices.reserve(quality);
		result.simplices.reserve(quality);

		for (std::size_t i = 0; i < quality; ++i)
		{
			T const a = (2 * static_cast<T>(pi) * i) / quality;
			geom::vector<T, 2> const v { std::cos(a), std::sin(a) };
			result.vertices.push_back(s.origin + v * s.radius);
		}

		for (Index i = 0; i < quality; ++i)
		{
			result.simplices.push_back({i, (i + 1) % static_cast<Index>(quality)});
		}

		return result;
	}

	template <typename Index = std::size_t, typename T>
	triangulation<geom::point<T, 3>, 2, Index> triangulate (sphere<T, 2> const & s, std::size_t quality = 32)
	{
		triangulation<geom::point<T, 3>, 2, Index> result;

		result.vertices.push_back(s.origin + s.radius * geom::vector<T, 3>{ T(), T(), T(-1) });
		result.vertices.push_back(s.origin + s.radius * geom::vector<T, 3>{ T(), T(), T(1) });

		for (std::size_t j = 1; j < quality; ++j)
		{
			T const b = (static_cast<T>(pi) * j) / quality;

			for (std::size_t i = 0; i < 2 * quality; ++i)
			{
				T const a = (static_cast<T>(pi) * i) / quality;

				geom::vector<T, 3> const v { std::cos(a) * std::sin(b), std::sin(a) * std::sin(b), -std::cos(b) };
				result.vertices.push_back(s.origin + s.radius * v);
			}
		}

		for (Index i = 0; i < 2 * quality; ++i)
		{
			for (Index j = 1; j + 1 < quality; ++j)
			{
				Index const k00 = 2 + (j - 1) * (2 * quality) + i;
				Index const k10 = 2 + (j - 1) * (2 * quality) + ((i + 1) % (2 * quality));
				Index const k01 = 2 + j * (2 * quality) + i;
				Index const k11 = 2 + j * (2 * quality) + ((i + 1) % (2 * quality));

				result.simplices.push_back({ k00, k10, k01 });
				result.simplices.push_back({ k10, k11, k01 });
			}
		}

		for (Index i = 0; i < 2 * quality; ++i)
		{
			Index const k0 = 2 + i;
			Index const k1 = 2 + ((i + 1) % (2 * quality));
			result.simplices.push_back({ 0, k1, k0 });
		}

		for (Index i = 0; i < 2 * quality; ++i)
		{
			Index const k0 = 2 + (quality - 2) * (2 * quality) + i;
			Index const k1 = 2 + (quality - 2) * (2 * quality) + ((i + 1) % (2 * quality));
			result.simplices.push_back({ k0, k1, 1 });
		}

		return result;
	}

}
