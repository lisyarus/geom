#pragma once

#include <geom/operations/range/rectangle.hpp>
#include <geom/operations/compare/vector.hpp>

#include <memory>
#include <array>

namespace geom
{

	namespace detail
	{

		template <typename T>
		using ndarray_storage = std::unique_ptr<T[]>;

		template <typename T>
		ndarray_storage<T> make_ndarray_storage (std::size_t size)
		{
			return ndarray_storage<T>(new T[size]);
		}

		template <typename T>
		ndarray_storage<T> make_ndarray_storage (std::size_t size, T const & default_value)
		{
			ndarray_storage<T> s(new T[size]);
			std::fill(s.get(), s.get() + size, default_value);
			return s;
		}

		template <typename T>
		T * ndarray_storage_data (ndarray_storage<T> & s)
		{
			return s.get();
		}

		template <typename T>
		T const * ndarray_storage_data (ndarray_storage<T> const & s)
		{
			return s.get();
		}

		template <std::size_t N, typename Array>
		std::size_t array_size_by_dimensions (Array const & dimensions)
		{
			std::size_t size = 1;
			for (std::size_t d : dimensions)
				size *= d;
			return size;
		}

		template <std::size_t N, typename Array1, typename Array2>
		std::size_t array_index (Array1 const & dimensions, Array2 const & index)
		{
			std::size_t i = 0;
			for (std::size_t n = N; n --> 0;)
			{
				i = i * dimensions[n] + index[n];
			}
			return i;
		}

		template <std::size_t N, typename T, typename Array1, typename Array2>
		ndarray_storage<T> array_resize (ndarray_storage<T> const & array, Array1 const & dimensions, Array2 const & new_dimensions)
		{
			std::array<std::size_t, N> copy_dimensions;
			for (std::size_t i = 0; i < N; ++i)
				copy_dimensions[i] = std::min(dimensions[i], new_dimensions[i]);

			ndarray_storage<T> new_array = make_ndarray_storage<T>(array_size_by_dimensions<N>(new_dimensions));

			geom::rectangle<std::size_t, N> rect;
			for (std::size_t i = 0; i < N; ++i)
				rect[i] = { std::size_t(0), copy_dimensions[i] };
			for (auto index: geom::range(rect))
			{
				new_array[array_index<N>(new_dimensions, index)] = array[array_index<N>(dimensions, index)];
			}

			return new_array;
		}

	}

	template <typename T, std::size_t N>
	struct ndarray
	{
		vector<std::size_t, N> dimensions;
		detail::ndarray_storage<T> storage;

		ndarray ( )
		{
			dimensions = dimensions.zero();
		}

		ndarray (vector<std::size_t, N> const & dimensions)
			: dimensions(dimensions)
			, storage(detail::make_ndarray_storage<T>(detail::array_size_by_dimensions<N>(dimensions)))
		{ }

		ndarray (vector<std::size_t, N> const & dimensions, T value)
			: dimensions(dimensions)
			, storage(detail::make_ndarray_storage<T>(detail::array_size_by_dimensions<N>(dimensions), value))
		{ }

		ndarray (ndarray const & other)
			: dimensions(other.dimensions)
		{
			std::size_t const storage_size = detail::array_size_by_dimensions<N>(dimensions);
			storage = detail::make_ndarray_storage<T>(storage_size);

			std::copy(other.data(), other.data() + storage_size, data());
		}

		ndarray (ndarray && other)
			: dimensions(other.dimensions)
			, storage(std::move(other.storage))
		{
			other.dimensions = other.dimensions.zero();
		}

		ndarray & operator = (ndarray const & other)
		{
			ndarray temp{other};
			temp.swap(*this);
			return *this;
		}

		ndarray & operator = (ndarray && other)
		{
			ndarray temp{std::move(other)};
			temp.swap(*this);
			return *this;
		}

		T * data ( )
		{
			return detail::ndarray_storage_data(storage);
		}

		T const * data ( ) const
		{
			return detail::ndarray_storage_data(storage);
		}

		void swap (ndarray & other)
		{
			std::swap(dimensions, other.dimensions);
			std::swap(storage, other.storage);
		}

		rectangle<std::size_t, N> rect ( ) const
		{
			rectangle<std::size_t, N> result;
			for (std::size_t i = 0; i < N; ++i)
				result[i] = { 0, dimensions[i] };
			return result;
		}

		void fill (T value = {})
		{
			storage.assign(storage.size(), value);
		}

		void resize (vector<std::size_t, N> const & dimensions)
		{
			if (this->dimensions != dimensions)
			{
				storage = detail::array_resize<N>(storage, this->dimensions, dimensions);
				this->dimensions = dimensions;
			}
		}

		template <typename Array, typename = std::enable_if_t<is_array<Array>::value>>
		T & operator[] (Array const & index)
		{
			return storage[detail::array_index<N>(dimensions, index)];
		}

		template <typename Array, typename = std::enable_if_t<is_array<Array>::value>>
		T const & operator[] (Array const & index) const
		{
			return storage[detail::array_index<N>(dimensions, index)];
		}

		template <typename Array, typename = std::enable_if_t<is_array<Array>::value>>
		T & at (Array const & index)
		{
			return storage[detail::array_index<N>(dimensions, index)];
		}

		template <typename Array, typename = std::enable_if_t<is_array<Array>::value>>
		T const & at (Array const & index) const
		{
			return storage[detail::array_index<N>(dimensions, index)];
		}

		template <typename ... Is>
		T & at (Is const & ... is)
		{
			return at(make_vector(is...));
		}

		template <typename ... Is>
		T const & at (Is const & ... is) const
		{
			return at(make_vector(is...));
		}

		template <typename ... Is>
		T & operator() (Is const & ... is)
		{
			return at(is...);
		}

		template <typename ... Is>
		T const & operator() (Is const & ... is) const
		{
			return at(is...);
		}
	};

}
