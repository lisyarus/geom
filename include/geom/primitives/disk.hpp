#pragma once

#include <geom/primitives/point.hpp>
#include <geom/primitives/triangulation.hpp>
#include <geom/utility.hpp>

namespace geom
{

	template <typename T>
	struct disk
	{
		geom::point<T, 2> origin;
		T radius;

		disk (geom::point<T, 2> origin = {T(), T()}, T radius = T(1))
			: origin(origin)
			, radius(radius)
		{ }
	};

	template <typename Index = std::size_t, typename T>
	triangulation<geom::point<T, 2>, 2, Index> triangulate (disk<T> const & c, std::size_t quality = 32)
	{
		triangulation<geom::point<T, 2>, 2, Index> result;
		result.vertices.reserve(quality + 1);
		result.simplices.reserve(quality);

		result.vertices.push_back(c.origin);
		for (std::size_t i = 0; i < quality; ++i)
		{
			T const a = (2 * static_cast<T>(pi) * i) / quality;
			geom::vector<T, 2> const v { std::cos(a), std::sin(a) };
			result.vertices.push_back(c.origin + v * c.radius);
		}

		for (Index i = 0; i < quality; ++i)
		{
			result.simplices.push_back({0, i + 1, (i + 1) % static_cast<Index>(quality) + 1});
		}

		return result;
	}

}
