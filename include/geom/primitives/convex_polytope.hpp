#pragma once

#include <geom/primitives/hyperplane.hpp>

#include <vector>

namespace geom
{

	// Intersection of a number of half-spaces
	template <typename T, std::size_t D>
	struct convex_polytope
	{
		std::vector<hyperplane<T, D>> planes;

		convex_polytope & operator += (vector<T, D> const & v);
		convex_polytope & operator -= (vector<T, D> const & v);
	};

	template <typename T, std::size_t D>
	convex_polytope<T, D> & convex_polytope<T, D>::operator += (vector<T, D> const & v)
	{
		for (auto & h : planes)
			h += v;
		return *this;
	}

	template <typename T, std::size_t D>
	convex_polytope<T, D> & convex_polytope<T, D>::operator -= (vector<T, D> const & v)
	{
		for (auto & h : planes)
			h -= v;
		return *this;
	}

	template <typename T, std::size_t D>
	convex_polytope<T, D> operator + (convex_polytope<T, D> c, vector<T, D> const & v)
	{
		c += v;
		return c;
	}

	template <typename T, std::size_t D>
	convex_polytope<T, D> operator + (vector<T, D> const & v, convex_polytope<T, D> c)
	{
		c += v;
		return c;
	}

	template <typename T, std::size_t D>
	convex_polytope<T, D> operator - (convex_polytope<T, D> c, vector<T, D> const & v)
	{
		c -= v;
		return c;
	}

}
