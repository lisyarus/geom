#pragma once

#include <geom/primitives/point.hpp>
#include <geom/primitives/vector.hpp>

namespace geom
{

	template <typename T, std::size_t D>
	struct ray
	{
		using vector_type = vector<T, D>;
		using point_type  = point <T, D>;

		point_type  origin;
		vector_type direction;

		point_type operator() (T t) const;

		ray & operator += (vector_type const & v);
		ray & operator -= (vector_type const & v);
	};

	template <typename T, std::size_t D>
	geom::point<T, D> ray<T, D>::operator() (T t) const
	{
		return origin + t * direction;
	}

	template <typename T, std::size_t D>
	ray<T, D> & ray<T, D>::operator += (vector_type const & v)
	{
		origin += v;
		return *this;
	}

	template <typename T, std::size_t D>
	ray<T, D> & ray<T, D>::operator -= (vector_type const & v)
	{
		origin -= v;
		return *this;
	}

	template <typename T, std::size_t D>
	ray<T, D> operator + (ray<T, D> const & r, vector<T, D> const & v)
	{
		ray rr = r;
		rr += v;
		return rr;
	}

	template <typename T, std::size_t D>
	ray<T, D> operator + (vector<T, D> const & v, ray<T, D> const & r)
	{
		ray rr = r;
		rr += v;
		return rr;
	}

	template <typename T, std::size_t D>
	ray<T, D> operator - (ray<T, D> const & r, vector<T, D> const & v)
	{
		ray rr = r;
		rr -= v;
		return rr;
	}

}
