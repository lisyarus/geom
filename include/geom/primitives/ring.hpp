#pragma once

#include <geom/primitives/point.hpp>
#include <geom/primitives/triangulation.hpp>
#include <geom/utility.hpp>

namespace geom
{

	template <typename T>
	struct ring
	{
		geom::point<T, 2> origin;
		T inner_radius, outer_radius;

		ring (geom::point<T, 2> origin = {T(), T()}, T inner_radius = T(0), T outer_radius = T(1))
			: origin(origin)
			, inner_radius(inner_radius)
			, outer_radius(outer_radius)
		{ }
	};

	template <typename Index = std::size_t, typename T>
	triangulation<geom::point<T, 2>, 2, Index> triangulate (ring<T> const & c, std::size_t quality = 32)
	{
		triangulation<geom::point<T, 2>, 2, Index> result;
		result.vertices.reserve(2 * quality);
		result.simplices.reserve(2 * quality);

		for (std::size_t i = 0; i < quality; ++i)
		{
			T const a = (2 * static_cast<T>(pi) * i) / quality;
			geom::vector<T, 2> const v { std::cos(a), std::sin(a) };
			result.vertices.push_back(c.origin + v * c.inner_radius);
			result.vertices.push_back(c.origin + v * c.outer_radius);
		}

		for (Index i = 0; i < quality; ++i)
		{
			result.simplices.push_back({2 * i, 2 * i + 1, (2 * i + 2) % static_cast<Index>(2 * quality)});
			result.simplices.push_back({2 * i + 1, (2 * i + 3) % static_cast<Index>(2 * quality), (2 * i + 2) % static_cast<Index>(2 * quality)});
		}

		return result;
	}

}
