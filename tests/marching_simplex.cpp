#include <unit_test.hpp>

#include <geom/algorithms/marching_simplex.hpp>
#include <geom/alias/point.hpp>
#include <geom/operations/point.hpp>
#include <geom/operations/simplex.hpp>
#include <geom/utility.hpp>

TEST_CASE(marching_simplex_2d)
{
	geom::triangulation<geom::point_2f, 2> t;

	int const n = 256;

	for (int y = 0; y <= n; ++y)
	{
		for (int x = 0; x <= n; ++x)
		{
			t.vertices.push_back({ float(x) / n, float(y) / n });
		}
	}

	for (int y = 0; y < n; ++y)
	{
		for (int x = 0; x < n; ++x)
		{
			std::size_t i00 = (y + 0) * (n + 1) + (x + 0);
			std::size_t i10 = (y + 0) * (n + 1) + (x + 1);
			std::size_t i01 = (y + 1) * (n + 1) + (x + 0);
			std::size_t i11 = (y + 1) * (n + 1) + (x + 1);

			t.simplices.push_back({i00, i10, i11});
			t.simplices.push_back({i00, i11, i01});
		}
	}

	auto f = [](geom::point_2f const & p){
		return p[0] * p[0] + p[1] * p[1] - 1.f;
	};

	auto check = [](geom::triangulation<geom::point_2f, 1> const & t)
	{
		float length = 0.f;
		for (auto s : t.simplices)
			length += geom::length(t.get(s));

		EXPECT_EQUAL_EPS(length * 2.f, geom::pi, 1e-5);
	};

	check(geom::marching_simplex(t, f));

	check(geom::marching_simplex(t, f, geom::merge_tag));
}
