#pragma once

#include <string>
#include <sstream>
#include <random>

namespace unit_test
{

	void add (void(*func)(), char const * name);

	void expect_true (bool value, std::string const & str);
	void expect_equal (bool equal, std::string const & str1, std::string const & str2);
	void expect_less (bool less, std::string const & str1, std::string const & str2);
	void expect_less_equal (bool less_equal, std::string const & str1, std::string const & str2);
	void expect_equal_eps (bool equal_eps, std::string const & str1, std::string const & str2, std::string const & eps, std::string const & real_diff);

	std::default_random_engine & rng ( );

	template <typename T>
	std::string to_string (T const & x)
	{
		std::ostringstream oss;
		oss << x;
		return oss.str();
	}

}

#define _TEST_CASE2(name, id)\
void test_##id##_func ( ); \
static int test_##id##_registrator = []{\
unit_test::add(&test_##id##_func, #name);\
return 0;\
}(); \
void test_##id##_func ( )

#define _TEST_CASE1(name, id) _TEST_CASE2(name, id)

#define TEST_CASE(name) _TEST_CASE1(name, __COUNTER__)

#define _EXPR_AND_VALUE(expr) #expr + std::string(" (value: ") + unit_test::to_string(expr) + ")"

#define EXPECT_TRUE(expr) unit_test::expect_true((expr), #expr);
#define EXPECT_EQUAL(expr1, expr2) unit_test::expect_equal((expr1) == (expr2), _EXPR_AND_VALUE(expr1), _EXPR_AND_VALUE(expr2));
#define EXPECT_LESS(expr1, expr2) unit_test::expect_less((expr1) < (expr2), _EXPR_AND_VALUE(expr1), _EXPR_AND_VALUE(expr2));
#define EXPECT_LESS_EQUAL(expr1, expr2) unit_test::expect_less_equal((expr1) <= (expr2), _EXPR_AND_VALUE(expr1), _EXPR_AND_VALUE(expr2));
#define EXPECT_EQUAL_EPS(expr1, expr2, eps) unit_test::expect_equal_eps(std::abs((expr1) - (expr2)) < eps, _EXPR_AND_VALUE(expr1), _EXPR_AND_VALUE(expr2), #eps, unit_test::to_string(std::abs((expr1) - (expr2))));
