#include <unit_test.hpp>

#include <geom/algorithms/contains/convex_point.hpp>
#include <geom/algorithms/convex_hull/andrew.hpp>
#include <geom/algorithms/convex_hull/graham.hpp>
#include <geom/algorithms/convex_hull/jarvis.hpp>

#include <geom/random/point.hpp>
#include <geom/alias/point.hpp>

#include <vector>

template <typename Hull>
void test_convex_hull (Hull && h, std::size_t count = 10)
{
	geom::random::uniform_disk_point_distribution<geom::point_2d> d({0.f, 0.f}, 1.f);

	while (count --> 0)
	{
		std::vector<geom::point_2d> points;
		for (std::size_t n = 0; n < 1000; ++n)
			points.push_back(d(unit_test::rng()));

		std::vector<geom::point_2d> hull;
		h(points.begin(), points.end(), std::back_inserter(hull));

		for (auto const & p : points)
			EXPECT_TRUE(geom::convex_contains(hull.begin(), hull.end(), p));
	}
}

TEST_CASE(andrew)
{
	test_convex_hull([](auto ... args){ geom::andrew_hull(args...); });
}

TEST_CASE(graham)
{
	test_convex_hull([](auto ... args){ geom::graham_hull(args...); });
}

TEST_CASE(jarvis)
{
	test_convex_hull([](auto ... args){ geom::jarvis_hull(args...); });
}
