#include <unit_test.hpp>

#include <geom/utility.hpp>

template <typename T, typename D>
void test_make_min (T a, T b, std::size_t count = 1000)
{
	D d(a, b);

	while (count --> 0)
	{
		T x = d(unit_test::rng());
		T y = d(unit_test::rng());

		if (x < y)
			std::swap(x, y);

		EXPECT_TRUE(geom::make_min(x, y));
		EXPECT_LESS_EQUAL(x, y);
	}
}

template <typename T, typename D>
void test_make_max (T a, T b, std::size_t count = 1000)
{
	D d(a, b);

	while (count --> 0)
	{
		T x = d(unit_test::rng());
		T y = d(unit_test::rng());

		if (x > y)
			std::swap(x, y);

		EXPECT_TRUE(geom::make_max(x, y));
		EXPECT_LESS_EQUAL(y, x);
	}
}

TEST_CASE(make_min_int)
{
	test_make_min<int, std::uniform_int_distribution<int>>(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
}

TEST_CASE(make_min_float)
{
	test_make_min<float, std::uniform_real_distribution<float>>(-1000.f, 1000.f);
}

TEST_CASE(make_max_int)
{
	test_make_max<int, std::uniform_int_distribution<int>>(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
}

TEST_CASE(make_max_float)
{
	test_make_max<float, std::uniform_real_distribution<float>>(-1000.f, 1000.f);
}
