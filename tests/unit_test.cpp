#include <unit_test.hpp>

#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <chrono>

namespace
{

	struct test_case
	{
		void (*func)();
		std::string name;
	};

	std::vector<test_case> & test_cases ( )
	{
		static std::vector<test_case> t;
		return t;
	}

	struct unit_test_exception
		: std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};

}

namespace unit_test
{

	using namespace std::literals;

	void add (void(*func)(), char const * name)
	{
		test_cases().push_back({func, name});
	}

	void expect_true (bool value, std::string const & str)
	{
		if (!value)
			throw unit_test_exception("Expected true: "s + str);
	}

	void expect_equal (bool equal, std::string const & str1, std::string const & str2)
	{
		if (!equal)
			throw unit_test_exception("Expected: "s + str1 + " is equal to "s + str2);
	}

	void expect_less (bool less, std::string const & str1, std::string const & str2)
	{
		if (!less)
			throw unit_test_exception("Expected: "s + str1 + " is less then "s + str2);
	}

	void expect_less_equal (bool less_equal, std::string const & str1, std::string const & str2)
	{
		if (!less_equal)
			throw unit_test_exception("Expected: "s + str1 + " is less then or equal to "s + str2);
	}

	void expect_equal_eps (bool equal_eps, std::string const & str1, std::string const & str2, std::string const & eps, std::string const & real_diff)
	{
		if (!equal_eps)
			throw unit_test_exception("Expected: "s + str1 + " and "s + str2 + " differ by at most "s + eps + ", but real difference is "s + real_diff);
	}

	std::default_random_engine & rng ( )
	{
		static std::default_random_engine r(std::chrono::system_clock::now().time_since_epoch().count());
		return r;
	}

}

int main ( )
{
	int ret = 0;
	for (std::size_t t = 0; t < test_cases().size(); ++t)
	{
		std::cout << "    Case #" << std::setw(3) << std::setfill(' ') << std::left << (t+1) << " ";
		std::cout << std::setw(30) << std::setfill('.') << std::left << test_cases()[t].name;
		try
		{
			test_cases()[t].func();
			std::cout << "success" << std::endl;
		}
		catch (unit_test_exception const & e)
		{
			std::cout << "failure:\n";
			std::cout << "      > " << e.what() << std::endl;
			ret = 1;
		}
		catch (std::exception const & e)
		{
			std::cout << "exception:\n";
			std::cout << "      > " << e.what() << std::endl;
			ret = 1;
		}
		catch (...)
		{
			std::cout << "unknown exception\n";
			ret = 1;
		}
	}
	return ret;
}
