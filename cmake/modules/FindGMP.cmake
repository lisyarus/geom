set(GMP_ROOT "" CACHE PATH "The path to the prefix of a GMP installation")

if (NOT "${GMP_FOUND}")
	include(FindPackageHandleStandardArgs)

	find_path(GMP_INCLUDE_DIR gmp.h
		PATHS ${GMP_ROOT}/include)

	find_library(GMP_LIBRARY NAMES gmp
		PATHS ${GMP_ROOT}/lib)

	find_package_handle_standard_args(GMP DEFAULT_MSG GMP_INCLUDE_DIR GMP_LIBRARY)

	if(GMP_FOUND)
		add_library(gmp SHARED IMPORTED)
		set_target_properties(gmp PROPERTIES
			IMPORTED_LOCATION ${GMP_LIBRARY}
			INTERFACE_INCLUDE_DIRECTORIES ${GMP_INCLUDE_DIR}
		)
	endif()

	mark_as_advanced(GMP_LIBRARY GMP_INCLUDE_DIR)
endif()
