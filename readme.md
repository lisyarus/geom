C++ geometric primitives & algorithms library.

Aimed at **genericity & code quality**, next *performance and precision*.

(yet, GCC is able to perfectly optimise everything)
