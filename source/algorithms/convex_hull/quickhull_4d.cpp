#include <geom/algorithms/convex_hull/quickhull_4d.hpp>

namespace geom
{

	template std::vector<std::array<std::size_t, 4>> quickhull (std::vector<geom::point<float, 4ul>> const & points);
	template std::vector<std::array<std::size_t, 4>> quickhull (std::vector<geom::point<double, 4ul>> const & points);

}
