#include <geom/algorithms/convex_hull/quickhull_3d.hpp>

namespace geom
{

	template std::vector<std::array<std::size_t, 3>> quickhull (std::vector<geom::point<float, 3ul>> const & points);
	template std::vector<std::array<std::size_t, 3>> quickhull (std::vector<geom::point<double, 3ul>> const & points);

}
