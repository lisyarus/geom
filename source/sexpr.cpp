#include <geom/sexpr.hpp>

#include <vector>

namespace geom
{

	namespace detail
	{

		namespace sexpr
		{

			namespace
			{

				inline int priority (unary_op_type op)
				{
					switch (op)
					{
					case unary_op_type::negation:
						return 1;
					}
				}

				inline int priority_right (unary_op_type op)
				{
					switch (op)
					{
					case unary_op_type::negation:
						return 3;
					}
				}

				inline int priority (binary_op_type op)
				{
					switch (op)
					{
					case binary_op_type::addition:
						return 2;
					case binary_op_type::substraction:
						return 2;
					case binary_op_type::multiplication:
						return 4;
					case binary_op_type::division:
						return 4;
					}
				}

				inline int priority_left (binary_op_type op)
				{
					switch (op)
					{
					case binary_op_type::addition:
						return 2;
					case binary_op_type::substraction:
						return 2;
					case binary_op_type::multiplication:
						return 4;
					case binary_op_type::division:
						return 4;
					}
				}

				inline int priority_right (binary_op_type op)
				{
					switch (op)
					{
					case binary_op_type::addition:
						return 2;
					case binary_op_type::substraction:
						return 3;
					case binary_op_type::multiplication:
						return 4;
					case binary_op_type::division:
						return 5;
					}
				}

				struct print_visitor
				{
					std::ostream & os;
					std::vector<int> priority_stack;

					print_visitor (std::ostream & os)
						: os(os)
						, priority_stack{0}
					{ }

					void visit (impl const & i)
					{
						boost::apply_visitor(*this, i);
					}

					void operator() (constant const & i)
					{
						os << i.value;
					}

					void operator() (variable const & i)
					{
						os << i.name;
					}

					void operator() (unary_op<impl> const & i)
					{
						if (priority(i.op) < priority_stack.back())
							os << '(';

						switch (i.op)
						{
						case unary_op_type::negation:
							os << '-';
							break;
						}

						priority_stack.push_back(priority_right(i.op));
						visit(i.arg1);
						priority_stack.pop_back();

						if (priority(i.op) < priority_stack.back())
							os << ')';
					}

					void operator() (binary_op<impl, impl> const & i)
					{
						if (priority(i.op) < priority_stack.back())
							os << '(';

						priority_stack.push_back(priority_left(i.op));
						visit(i.arg1);
						priority_stack.pop_back();

						switch (i.op)
						{
						case binary_op_type::addition:
							os << '+';
							break;
						case binary_op_type::substraction:
							os << '-';
							break;
						case binary_op_type::multiplication:
							os << '*';
							break;
						case binary_op_type::division:
							os << '/';
							break;
						}

						priority_stack.push_back(priority_right(i.op));
						visit(i.arg2);
						priority_stack.pop_back();

						if (priority(i.op) < priority_stack.back())
							os << ')';
					}
				};

			}

			impl simplify (impl i)
			{
				if (auto * pi = boost::get<unary_op<impl>>(&i))
				{
					auto arg1 = simplify(pi->arg1);

					switch (pi->op)
					{
					case unary_op_type::negation:
					{
						// negation(constant) = -constant
						if (auto * pi1 = boost::get<constant>(&arg1))
							return constant{-pi1->value};

						if (auto * pi1 = boost::get<unary_op<impl>>(&arg1))
						{
							switch (pi1->op)
							{
							// negation(negation) = id
							case unary_op_type::negation:
								return simplify(pi1->arg1);
							}
						}

						if (auto * pi1 = boost::get<binary_op<impl, impl>>(&arg1))
						{
							switch (pi1->op)
							{
							// negation(x+y) = negation(x)-y
							case binary_op_type::addition:
								return simplify(make_binary_op(make_unary_op(simplify(pi1->arg1), unary_op_type::negation), simplify(pi1->arg2), binary_op_type::substraction));
							// negation(x-y) = negation(x)+y
							case binary_op_type::substraction:
								return simplify(make_binary_op(make_unary_op(simplify(pi1->arg1), unary_op_type::negation), simplify(pi1->arg2), binary_op_type::addition));
							}
						}

						break;
					}
					}
				}

				if (auto * pi = boost::get<binary_op<impl, impl>>(&i))
				{
					auto arg1 = simplify(pi->arg1);
					auto arg2 = simplify(pi->arg2);

					switch (pi->op)
					{
					case binary_op_type::addition:
					{
						if (auto * pi1 = boost::get<constant>(&arg1))
						{
							// 0+x = x
							if (pi1->value == 0)
								return arg2;

							// constant+constant = constant
							if (auto * pi2 = boost::get<constant>(&arg2))
							{
								return constant{pi1->value + pi2->value};
							}
						}

						if (auto * pi2 = boost::get<constant>(&arg2))
						{
							// x+0 = x
							return arg1;
						}

						break;
					}
					case binary_op_type::substraction:
					{
						if (auto * pi1 = boost::get<constant>(&arg1))
						{
							// 0-x = -x
							if (pi1->value == 0)
								return simplify(make_unary_op(arg2, unary_op_type::negation));

							// constant-constant = constant
							if (auto * pi2 = boost::get<constant>(&arg2))
							{
								return constant{pi1->value - pi2->value};
							}
						}

						if (auto * pi2 = boost::get<constant>(&arg2))
						{
							// x-0 = x
							return arg1;
						}

						break;
					}
					}
				}

				return i;
			}

		}

	}

	std::ostream & operator << (std::ostream & os, sexpr const & e)
	{
		detail::sexpr::print_visitor v{os};
		v.visit(e.impl);
		return os;
	}

}
